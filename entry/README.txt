﻿I, Castaway
===========

CONTACT: See homepage for more information.

Homepage: https://pyweek.org/e/pw30/
Name: 'I, Castaway'
Team: ¯\_(ツ)_/¯
Members:  DR0ID, Gummbum


DEPENDENCIES:

You might need to install some of these before running the game:

  Python (3.7):       http://www.python.org/
  PyGame (1.9.6):     http://www.pygame.org/

  Note: other versions not tested. Still, it may work. pygame2 does not work perfectly.


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run.py" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run.py

  -or in some cases-

  py3 run.py

Note: You may need to: pip install pygame.


HOW TO PLAY THE GAME:

I, Castaway is an interactive story game of discovery. Take your time and read
the speech bubbles. In its entirety the game should only take about five (5)
minutes to play through.

Starting:

1. Use A,S,W,D to move around.

Advancing:

The following skills will be unlocked when you complete the objectives:
1. Use Left-Mouse to jump far distances with great accuracy.
2. Use Right-Mouse to double-jump (a small, inaccurate boost - you may need it).

End game:

There wasn't time for the graceful ending or clever credits-outro that we
usually go for. When you reach the final site where you can imagine being
rescued, you've seen everything in the game except for some very remote
easter eggs. The end will be obvious enough: the shiny "next goal" indicator
remains where you're standing, and the hero does not have any more glib humor
to dispense. You may close the game window at this point, or go exploring. If
you find anything of interest, please report your findings to Central Command,
attn: DR0ID and Gumm via Discord or pyweek.org bulletin board.


LICENSE:

This game's skellington and other code have the same license as pyknic.

Sound and graphics assets are freely available, some with restrictions.
Attributions, where known, are in the various data directories.


