<?xml version="1.0" encoding="UTF-8"?>
<tileset name="astronaut" tilewidth="128" tileheight="128" tilecount="48" columns="8">
 <image source="../graphics/astronaut.png" width="1024" height="768"/>
 <tile id="0" type="Animation">
  <properties>
   <property name="kind" value="anim_hero_walk"/>
  </properties>
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="12" duration="100"/>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="15" duration="100"/>
  </animation>
 </tile>
 <tile id="1" type="Animation">
  <properties>
   <property name="kind" value="anim_hero_jump"/>
  </properties>
  <animation>
   <frame tileid="16" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="18" duration="100"/>
  </animation>
 </tile>
 <tile id="8" type="Astronaut"/>
 <tile id="9" type="Astronaut"/>
 <tile id="10" type="Astronaut"/>
 <tile id="11" type="Astronaut"/>
 <tile id="12" type="Astronaut"/>
 <tile id="13" type="Astronaut"/>
 <tile id="14" type="Astronaut"/>
 <tile id="15" type="Astronaut"/>
 <tile id="16" type="Astronaut"/>
 <tile id="17" type="Astronaut"/>
 <tile id="18" type="Astronaut"/>
 <tile id="19" type="Astronaut"/>
 <tile id="20" type="Astronaut"/>
</tileset>
