<?xml version="1.0" encoding="UTF-8"?>
<tileset name="desert1" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="../graphics/desert1.png" width="512" height="512"/>
 <tile id="60" type="Herbs">
  <properties>
   <property name="strength" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="61" type="Herbs">
  <properties>
   <property name="strength" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="62" type="Herbs">
  <properties>
   <property name="strength" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
