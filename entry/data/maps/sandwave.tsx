<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sandwave" tilewidth="60" tileheight="60" tilecount="10" columns="10">
 <image source="../graphics/sandwave60x60.png" width="600" height="60"/>
 <tile id="7" type="Animation">
  <properties>
   <property name="kind" value="anim_wave"/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
  </animation>
 </tile>
</tileset>
