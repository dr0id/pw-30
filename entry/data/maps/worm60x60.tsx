<?xml version="1.0" encoding="UTF-8"?>
<tileset name="worm60x60" tilewidth="60" tileheight="60" tilecount="20" columns="10">
 <image source="../graphics/worm60x60.png" width="600" height="120"/>
 <tile id="0" type="Worm"/>
 <tile id="1" type="Worm"/>
 <tile id="2" type="Worm"/>
 <tile id="3" type="Worm"/>
 <tile id="4" type="Worm"/>
 <tile id="5" type="Worm"/>
 <tile id="6" type="Worm"/>
 <tile id="7" type="Worm"/>
 <tile id="8" type="Worm"/>
 <tile id="9" type="Worm"/>
 <tile id="10" type="Animation">
  <properties>
   <property name="kind" value="anim_worm_eat"/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
   <frame tileid="7" duration="100"/>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="1" duration="100"/>
  </animation>
 </tile>
 <tile id="11" type="Animation">
  <properties>
   <property name="kind" value="anim_worm_rumble"/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="1" duration="100"/>
  </animation>
 </tile>
 <tile id="12" type="Animation">
  <properties>
   <property name="kind" value="anim_goat"/>
  </properties>
  <animation>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="13" duration="100"/>
   <frame tileid="15" duration="100"/>
  </animation>
 </tile>
 <tile id="13" type="Goat"/>
 <tile id="14" type="Goat"/>
 <tile id="15" type="Goat"/>
 <tile id="19">
  <properties>
   <property name="kind" value="jetpack"/>
  </properties>
 </tile>
</tileset>
