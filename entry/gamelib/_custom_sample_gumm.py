# -*- coding: utf-8 -*-
from __future__ import print_function

import logging
from collections import namedtuple

from gamelib import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

custom = {
    'hero': False,
    'energy': False,
    'fart': True,
    'map': True,
    'debug_skip_bg_tiles': False,
}

if custom['hero']:
    settings.hero_params = settings.HeroParams(
        speed=15, jump_speed=1000, jump_height=2, fart_angle_inaccuracy_in_deg=45, discharge_th_s=2)

if custom['energy']:
    settings.energy_params = settings.EnergyParams(max_energy=500, regeneration_rate=500)

if custom['fart']:
    settings.fart_params = settings.GasOMeterParams(
        max_level=100, critical_level=60, explode_chance=0.01, generation_rate=2, range_extension=0.5)

if custom['map']:
    settings.current_map = settings.MAP_TEST_3

if custom['debug_skip_bg_tiles']:
    settings.debug_skip_bg_tiles = True

logger.debug("imported")
