# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameplayContext"]  # list of public visible parts of this module

import random

from collections import defaultdict

import pygame

from gamelib import settings
from gamelib.gamelogic import worm, critter, hero
from gamelib.gamelogic.critter import CritterDead
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.gamelogic.hero import HeroDead
from gamelib.gamerender.huds import GameHUD
from gamelib.gamerender import spritefx, game_texts, pygametext
from pyknic.animation import Animation
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite, VectorSprite
from pyknic.mathematics import Point3 as Point
from pyknic.mathematics import Vec3 as Vec
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


class EntitySprite(Sprite):

    def __init__(self, entity, image, position, anchor=None, z_layer=None, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        Sprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init, name)
        self.entity = entity


class AnimatedSprite(Sprite, Animation):

    def __init__(self, frames, fps, scheduler, position, z_layer):
        Animation.__init__(self, 0, len(frames), fps, scheduler)
        Sprite.__init__(self, frames[0], position, z_layer=z_layer)
        self.event_index_changed += self._idx_changed
        self.frames = frames
        self.visible = False

    def _idx_changed(self, *args):
        self.set_image(self.frames[self.current_index])


class CircleSprite(Sprite):

    def __init__(self, the_hero, z_layer, color):
        self.draw_special = True
        self.dirty_update = False
        Sprite.__init__(self, None, the_hero.position, z_layer=z_layer)
        self.color = color
        self.hero = the_hero

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        if self.hero.energy_indicator.current_energy < 1:
            return pygame.Rect(0, 0, 0, 0)
        center = cam.world_to_screen(self.position).as_xy_tuple(int)
        return pygame.draw.circle(surf,
                                  self.color,
                                  center,
                                  int(self.hero.energy_indicator.current_energy) + 1,
                                  1)

class Particle(Sprite):

    def __init__(self, img, position, z_layer, velocity, duration, zoom_range, rot_speed):
        Sprite.__init__(img, position, z_layer)
        self.velocity = velocity
        self.t = duration
        self.duration = duration
        self.zoom_range = zoom_range
        self.rot_speed = rot_speed


class ParticleEmitter(object):
    def __init__(self, position, frames, renderer, speed, rot_speed, creation_rate):
        self.creation_rate = creation_rate
        self.frames = frames
        self.position = position
        self.particles = []
        self.renderer = renderer
        self.speed = speed
        self.rot_speed = rot_speed


    def update(self, dt):
        for p in self.particles:
            p.t -= dt
            if p.t <= 0:
                self.particles.remove(p)
                self.renderer.remove_sprite(p)
            live = p.t / p.duration
            p.position += p.velocity * dt
            p.velocity += p.acc * dt
            p.zoom = (1 - live) * p.zoom_range + 1 # zoom_start
            p.alpha *= live
            p.rotation += p.rot_speed

    def create_particles(self, count):
        for i in range(count):
            self.create_particle()

    def create_particle(self):
        img = random.choice(self.frames)
        position = self.position.clone()
        velocity = Vec(1, 0)
        velocity.rotate(random.randint(0, 360))
        velocity *= self.speed
        z_layer = settings.layer_hero - 0.5
        p = Particle(img, position, z_layer, velocity, self.duration, self.zoom_range, self.rot_speed)
        self.particles.append(p)
        self.renderer.add_sprite(p)


class GameplayContext(TimeSteppedContext):

    def __init__(self, game_logic):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.game_logic = game_logic
        self.resources = None
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point(0, 0))
        self.screen = None
        self._is_debug_render_on = False
        self._debug_sprites = []
        self.level = None
        self.hud = GameHUD()
        self.render_clock = pygame.time.Clock()
        self.logic_clock = pygame.time.Clock()
        self.animations = defaultdict()  # {ent:{state:anim}}
        self.scheduler = Scheduler()
        self.cursor_sprite = None
        self.cursor_pos = Point(0, 0)
        self.target_sprite = None
        self.tweener = Tweener()
        self.clue_blink_sprite = None

    def enter(self):
        pygame.mouse.set_visible(False)
        event_dispatcher.register_event_type(settings.EVT_NOISE)
        event_dispatcher.register_event_type(settings.EVT_GOAT_STATE_CHANGE)
        event_dispatcher.register_event_type(settings.EVT_WORM_STATE_CHANGE)
        event_dispatcher.register_event_type(settings.EVT_HERO_STATE_CHANGE)
        event_dispatcher.register_event_type(settings.EVT_ENTITY_REMOVED)

    def exit(self):
        self.game_logic.exit()
        self.resources.clear()
        pygame.mouse.set_visible(True)
        # event_dispatcher.clear()  # todo need to clear all events that might still be in the queue and remove all listeners?

    def suspend(self):
        self.game_logic.suspend()

    def resume(self):
        if self.level is None:
            self.resources = resource_manager.resources
            self.screen = pygame.display.get_surface()

            map_resource = resource_manager.resources[settings.resource_map1]

            assert settings.KIND_HERO in (e.kind for e in map_resource.level.entities)
            assert settings.KIND_START in (e.kind for e in map_resource.level.entities)
            assert settings.KIND_END in (e.kind for e in map_resource.level.entities)

            assert settings.KIND_ANIM_GOAT in map_resource.level.animations
            assert settings.KIND_ANIM_HERO_JUMP in map_resource.level.animations
            assert settings.KIND_ANIM_HERO_WALK in map_resource.level.animations
            assert settings.KIND_ANIM_WAVE in map_resource.level.animations
            assert settings.KIND_ANIM_WORM_EAT in map_resource.level.animations
            assert settings.KIND_ANIM_WORM_RUMBLE in map_resource.level.animations

            self.level = map_resource.level

            self.game_logic.initialize_entities(map_resource.entities.keys())
            self.cam.set_position(self.game_logic.hero.position)

            sprites_to_add = map_resource.sprites + self._debug_sprites
            self.renderer.add_sprites(sprites_to_add)

            # setup animations
            _d = {"current": None}
            for ent in map_resource.entities:
                if ent.kind == settings.KIND_GOAT:
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_GOAT]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_goat)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_GOAT] = anim
                    anim = AnimatedSprite([frames[0]], fps, self.scheduler, ent.position, settings.layer_goat)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_GOAT_STAND] = anim
                    self._goat_state_change(ent)
                elif ent.kind == settings.KIND_HERO:
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_HERO_WALK]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_hero)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_HERO_WALK] = anim
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_HERO_JUMP]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_hero)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_HERO_JUMP] = anim
                    anim = AnimatedSprite([frames[1]], fps, self.scheduler, ent.position, settings.layer_hero)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_HERO_STAND] = anim
                    self._hero_state_change(ent)
                elif ent.kind == settings.KIND_WORM:
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_WAVE]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_worm)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_WAVE] = anim
                    anim = AnimatedSprite(frames, fps * 1.5, self.scheduler, ent.position, settings.layer_worm)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_WAVE_SEEK] = anim
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_WORM_EAT]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_worm)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_WORM_EAT] = anim
                    frames, fps = map_resource.level.animations[settings.KIND_ANIM_WORM_RUMBLE]
                    anim = AnimatedSprite(frames, fps, self.scheduler, ent.position, settings.layer_worm)
                    self.animations.setdefault(ent, dict(_d))[settings.KIND_ANIM_WORM_RUMBLE] = anim
                    self._worm_state_change(ent)

            # setup state change listeners
            event_dispatcher.add_listener(settings.EVT_GOAT_STATE_CHANGE, self._goat_state_change)
            event_dispatcher.add_listener(settings.EVT_HERO_STATE_CHANGE, self._hero_state_change)
            event_dispatcher.add_listener(settings.EVT_WORM_STATE_CHANGE, self._worm_state_change)
            event_dispatcher.add_listener(settings.EVT_KILLED, self.killed_entity)
            event_dispatcher.add_listener(settings.EVT_ENTITY_REMOVED, self.entity_removed)

            # cursor
            cursor_resource = resource_manager.resources[settings.resource_cursor]
            self.cursor_sprite = Sprite(cursor_resource.image, self.cursor_pos, z_layer=settings.layer_cursor)
            self.renderer.add_sprite(self.cursor_sprite)

            target_resource = resource_manager.resources[settings.resource_target]
            self.target_sprite = Sprite(target_resource.image, self.game_logic.hero.target,
                                        z_layer=settings.layer_cursor)
            self.renderer.add_sprite(self.target_sprite)

            circle = CircleSprite(self.game_logic.hero, settings.layer_cursor, settings.cursor_color)
            self.renderer.add_sprite(circle)

            blink_res = resource_manager.resources[settings.resource_clue_blink]
            self.clue_blink_sprite = Sprite(blink_res.image, self.game_logic.next_clue_entity.position, z_layer=settings.layer_clue_blink)
            self.renderer.add_sprite(self.clue_blink_sprite)
            self.scheduler.schedule(self._blink_clue, 4)
            self._blink_clue()

            self.game_logic.start_opening_credits()




            # todo maybe if we have time
            # black_smoke = resource_manager.resources[settings.resource_black_smoke]
            # white_smoke = resource_manager.resources[settings.resource_white_smoke]
            # fart_particles = resource_manager.resources[settings.resource_fart]
            #
            # p = ParticleEmitter(black_smoke.frames)



            # proof of concept that a background can be done with one single surface for repeating all the same tiles
            # tw = 50
            # th = 80
            # sw = self.cam.rect.width + tw
            # sh = self.cam.rect.height + th
            # surf = pygame.Surface((sw, sh))
            # surf.fill((255, 255, 0))
            # for x in range(0, sw+tw, tw):
            #     for y in range(0, sh+th, th):
            #         surf.fill((255, 0, 0), (x, y, 5, 5))
            #         surf.fill((255, 0, 0), (x+30, y, 5, 5))
            #         surf.fill((255, 0, 0), (x+30, y+20, 5, 5))
            # self.bg_spr = Sprite(surf, self.cam.position, z_layer=1000000)
            # resource_manager.resources[settings.resource_map1].sprites.append(self.bg_spr)
    def _blink_clue(self, *args):
        tw = self.tweener.create_tween_by_end(self.clue_blink_sprite, 'zoom', 0.0, 1.0, 0.5, do_start=False)\
            .next(self.tweener.create_tween_by_end(self.clue_blink_sprite, 'zoom', 1.0, 0.0, 0.5, do_start=False))
        tw.start()
        return 4  # blink every 4 seconds

    def entity_removed(self, ent):
        sprites = [spr for spr in self.renderer.get_sprites() if hasattr(spr, "entity") and spr.entity == ent]
        self.renderer.remove_sprites(sprites)

    def killed_entity(self, ent):
        if ent == self.game_logic.hero:
            self.display_death_message()

        anims = self.animations.get(ent, None)
        if anims:
            for a in anims.values():
                a.stop()
                a.visible = False
            # self.renderer.remove_sprites(anims.values())
            # del self.animations[ent]

    def _goat_state_change(self, goat):
        if goat.state_machine.current_state == CritterDead:
            return
        anims = self.animations.get(goat, None)
        if anims:
            a = anims[settings.KIND_ANIM_GOAT_STAND]
            if goat.state_machine.current_state != critter.CritterStand:
                a = anims[settings.KIND_ANIM_GOAT]
            self._update_animations(a, goat)

    def _update_animations(self, to_activate, ent):
        a = self.animations[ent]["current"]
        if a:
            a.stop()
            a.visible = False

        to_activate.start()
        to_activate.visible = True
        self.animations[ent]["current"] = to_activate

    def _hero_state_change(self, the_hero):
        if the_hero.state_machine.current_state == HeroDead:
            return

        hero_anims = self.animations.get(the_hero, None)
        if hero_anims:
            a = hero_anims[settings.KIND_ANIM_HERO_STAND]
            if the_hero.state_machine.current_state == hero.HeroWalkState:
                a = hero_anims[settings.KIND_ANIM_HERO_WALK]
            elif the_hero.state_machine.current_state == hero.HeroInAir:
                a = hero_anims[settings.KIND_ANIM_HERO_JUMP]
            elif the_hero.state_machine.current_state == hero.HeroDoubleJump:
                a = hero_anims[settings.KIND_ANIM_HERO_JUMP]

            self._update_animations(a, the_hero)

    def _worm_state_change(self, the_worm):
        a = self.animations[the_worm][settings.KIND_ANIM_WAVE]
        if the_worm.state_machine.current_state == worm.WormWanderSubmergedState \
                or the_worm.state_machine.current_state == worm.WormDipState:
            for a in self.animations[the_worm].values():
                a.stop()
                a.visible = False
                return
        elif the_worm.state_machine.current_state == worm.WormSeekState:
            a = self.animations[the_worm][settings.KIND_ANIM_WAVE_SEEK]
        elif the_worm.state_machine.current_state == worm.WormAttackState:
            a = self.animations[the_worm][settings.KIND_ANIM_WORM_RUMBLE]
        elif the_worm.state_machine.current_state == worm.WormKillState:
            a = self.animations[the_worm][settings.KIND_ANIM_WORM_EAT]

        self._update_animations(a, the_worm)

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):  # fixme: AFTER_PYWEEK this dt is wrong...
        # logger.debug(f"draw dt {dt}, sim_t {sim_dt}")
        for ent in self.game_logic.entities:
            a = self.animations.get(ent, None)
            if a:
                a["current"].rotation = -ent.direction.angle
        hero_anims = self.animations.get(self.game_logic.hero, None)
        if hero_anims:
            factor =  self.game_logic.hero.position.z / settings.hero_params.jump_height
            if factor > 2:
                logger.error("what!? zoom factor way too big %s", factor)
                factor = 2.0

            hero_anims["current"].zoom = factor * 0.5 + 0.3

        # hud
        self.hud.set_hero_pos(self.game_logic.hero.position)
        self.hud.set_camera_pos(self.cam.position)
        self.hud.set_bg_tiles(len(self.renderer.get_sprites()))
        self.hud.set_fps(self.render_clock.get_fps())
        self.hud.set_frames(self.logic_clock.get_fps())
        self.hud.set_milestone(settings._global_id_generator.names.get(settings.milestones[-1], 'None'))
        self.hud.update()

        # proof of concept that a background can be done with one single surface for repeating all the same tiles
        # self.bg_spr.offset.x = self.cam.position.x % 50 - 25 #+ self.cam.rect.w // 2
        # self.bg_spr.offset.y = self.cam.position.y % 80 - 40 #+ self.cam.rect.h // 2

        diff = self.game_logic.next_clue_entity.position - self.game_logic.hero.position
        if diff.length_sq > 500 * 500:
            diff.length = 500
            self.clue_blink_sprite.position.copy_values(self.game_logic.hero.position + diff)
        else:
            self.clue_blink_sprite.position.copy_values(self.game_logic.next_clue_entity.position)

        # draw to screen!
        self.renderer.draw(self.screen, self.cam, (0, 0, 0), False)

        for sprite in spritefx.text_sprites:
            sprite.draw(self.screen)
        # if spritefx.text_sprites:
        #     logger.error('text_sprites={}', spritefx.text_sprites)
        if game_texts.speech_bubbles:
            game_texts.speech_bubbles[0].sprite.draw(self.screen)
            # logger.error('+++ Bubble={}', tuple([b.text for b in game_texts.speech_bubbles]))

        if self._is_debug_render_on:
            self._draw_debug()

        self.hud.draw(self.screen)

        # gauges
        gauge_height = 300
        ei = self.game_logic.hero.energy_indicator
        pos = 10, settings.screen_height - 10
        if ei.params != settings.no_energy_params:
            current_level = int(gauge_height * ei.current_energy / ei.params.max_energy)
            r = pygame.Rect(pos, (20, current_level))
            r.bottomleft = pos
            pygame.draw.rect(self.screen, settings.cursor_color, r)
            r.height = gauge_height
            r.bottomleft = pos
            pygame.draw.rect(self.screen, (255, 255, 255), r, 1)

        pos = 40, settings.screen_height - 10
        gm = self.game_logic.hero.gas_o_meter
        if gm.params != settings.no_fart_params:
            current_level = int(gauge_height * gm.current_level / gm.params.max_level)
            r = pygame.Rect(40, settings.screen_height - gauge_height, 20, current_level)
            r.bottomleft = pos
            pygame.draw.rect(self.screen, settings.flatulence_color, r)

            if gm.current_level > gm.params.critical_level:
                _y = int(gauge_height * gm.params.critical_level / gm.params.max_level)
                _h = gauge_height - _y
                level = gm.current_level - gm.params.critical_level
                crit_h = gm.params.max_level - gm.params.critical_level
                r.height = int(_h * level / crit_h)
                r.bottomleft = pos[0], pos[1] - _y
                pygame.draw.rect(self.screen, settings.flatulence_critical_color, r)


            r.height = gauge_height
            r.bottomleft = pos
            pygame.draw.rect(self.screen, (255, 255, 255), r, 1)


        if do_flip:
            pygame.display.flip()

        self.render_clock.tick()

    def update_step(self, delta, sim_time, *args):
        # logger.debug(f"update_step dt {delta}, sim_t {sim_time}")
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

        self.clue_blink_sprite.rotation += 360 * delta

        self.handle_events()
        self.game_logic.update(delta, sim_time, *args)

        # cam control
        t = settings.cam_speed * delta
        self.cam.lerp(self.game_logic.hero.position, t)

        self.logic_clock.tick()

    def handle_events(self):
        actions, unmapped_events = settings.gameplay_event_mapper.get_actions(pygame.event.get())
        world_actions = []
        for action, extra in actions:
            if action == settings.ACTION_QUIT:
                self.pop()
            elif action == settings.ACTION_TOGGLE_DEBUG_RENDER:
                self._is_debug_render_on = not self._is_debug_render_on
                self._update_debug_sprites()
            elif action == settings.ACTION_HERO_FIRE:
                sx, sy = extra
                extra = self.cam.screen_to_world_comp(sx, sy)
            elif action == settings.ACTION_HERO_AIM:
                # event.pos, event.rel, event.buttons)
                pos, rel, buttons = extra
                sx, sy = pos
                world_pos = self.cam.screen_to_world_comp(sx, sy)
                self.cursor_pos.copy_values(world_pos)
                extra = world_pos, Vec(rel[0], rel[1]), buttons

            world_actions.append((action, extra))
        self.game_logic.handle_actions(world_actions)

    def _draw_debug(self):
        hero_vel = [spr for spr in self._debug_sprites if spr.name == "hero_vel"][0]
        hero_vel.vector.copy_values(self.game_logic.hero.velocity)
        hero_vel.vector.length *= 1
        hero_dir = [spr for spr in self._debug_sprites if spr.name == "hero_dir"][0]
        hero_dir.vector.copy_values(self.game_logic.hero.direction)
        hero_dir.vector.length = 30
        hero_spr = [spr for spr in self._debug_sprites if spr.name == "hero"][0]
        hero_spr.zoom = (self.game_logic.hero.position.z / settings.hero_params.jump_height) * 0.5 + 0.5
        center = self.cam.world_to_screen(self.game_logic.hero.position).as_xy_tuple(int)
        pygame.draw.circle(self.screen,
                           (0, 255, 0),
                           center,
                           int(self.game_logic.hero.energy_indicator.current_energy) + 1,
                           1)
        gas_o_meter = self.game_logic.hero.gas_o_meter
        r = pygame.Rect(center[0], center[1] - 40, gas_o_meter.current_level, 10)
        color = (0, 0, 200) if gas_o_meter.current_level < gas_o_meter.params.critical_level else (200, 0, 0)
        pygame.draw.rect(self.screen, color, r)
        r.w = gas_o_meter.params.max_level
        pygame.draw.rect(self.screen, (255, 255, 255), r, 1)

        pygame.draw.circle(self.screen, (0, 255, 0),
                           self.cam.world_to_screen(self.game_logic.hero.target).as_xy_tuple(int), 5)

        # worm
        for _worm in (_e for _e in self.game_logic.entities if _e.kind == settings.KIND_WORM):
            worm_target = self.cam.world_to_screen(_worm.target)
            worm_pos = self.cam.world_to_screen(_worm.position)
            worm_screen_pos = worm_pos.as_xy_tuple(int)

            worm_color = (200, 0, 0)
            if _worm.state_machine.current_state == worm.WormDipState or _worm.state_machine.current_state == worm.WormWanderSubmergedState:
                worm_color = (50, 0, 0)
            elif _worm.state_machine.current_state == worm.WormAttackState:
                worm_color = (255, 0, 0)
            pygame.draw.circle(self.screen, worm_color, worm_screen_pos, 25)
            pygame.draw.line(self.screen, (255, 255, 255), worm_screen_pos, worm_target.as_xy_tuple(int))
            pygame.draw.line(self.screen, (0, 255, 0), worm_screen_pos,
                             (worm_pos + _worm.direction * 10).as_xy_tuple(int))
            pygame.draw.circle(self.screen, (200, 200, 200), worm_screen_pos, int(_worm.radius), 1)
            pygame.draw.circle(self.screen, (200, 200, 0), worm_screen_pos, int(_worm.params.seek_dip_dist_sq ** 0.5),
                               1)
            pygame.draw.circle(self.screen, (200, 200, 150), worm_screen_pos,
                               int(_worm.params.max_hear_distance_sq ** 0.5),
                               1)

        # rock
        for rock in (r for r in self.game_logic.entities if r.kind == settings.KIND_SAFE_ROCK):
            rock_pos = self.cam.world_to_screen(rock.position).as_xy_tuple(int)
            pygame.draw.circle(self.screen, (200, 200, 200), rock_pos, int(rock.radius), 1)

        # critters
        for crit in (c for c in self.game_logic.entities if c.kind == settings.KIND_GOAT):
            pos = self.cam.world_to_screen(crit.position).as_xy_tuple(int)
            pygame.draw.circle(self.screen, (0, 200, 0), pos, 20)

        # TODO: gumm
        # start
        matches = list(e for e in resource_manager.resources[settings.resource_map1].entities
                       if e.kind in (settings.KIND_START, settings.KIND_END, settings.KIND_CLUE))
        for ent in matches:
            rect = ent.rect
            new_pos = self.cam.world_to_screen(ent.position)
            rect.center = new_pos[:2]
            pygame.draw.rect(self.screen, pygame.Color('green'), rect, 1)

        # cam center position
        pygame.draw.circle(self.screen, (50, 50, 50), self.cam.world_to_screen(self.cam.position).as_xy_tuple(int), 5)

    def _update_debug_sprites(self):
        if self._is_debug_render_on:
            # add debug sprites
            if not self._debug_sprites:
                hero_sprite = Sprite(self.resources[settings.resource_hero].image, self.game_logic.hero.position,
                                     name="hero", z_layer=settings.layer_debug + 10)

                self._debug_sprites = [
                    VectorSprite(Vec(10, 0), Point(0, 0), (255, 0, 0), z_layer=settings.layer_debug + 1),
                    VectorSprite(Vec(0, 10), Point(0, 0), (0, 255, 0), z_layer=settings.layer_debug + 1),
                    VectorSprite(Vec(30, 0), self.game_logic.hero.position, (255, 255, 255),
                                 z_layer=settings.layer_debug + 11, name="hero_dir"),
                    VectorSprite(Vec(30, 0), self.game_logic.hero.position, (0, 0, 255),
                                 z_layer=settings.layer_debug + 11, name="hero_vel"),
                    hero_sprite,
                ]

                for y in range(-500, 500, 100):
                    for x in range(-500, 500, 100):
                        self._debug_sprites.append(
                            VectorSprite(Vec(3, 0), Point(x, y), (0, 150, 200), z_layer=settings.layer_debug))

                for worm in (_e for _e in self.game_logic.entities if _e.kind == settings.KIND_WORM):
                    worm_sprite = Sprite(self.resources[settings.resource_worm].image, worm.position,
                                         z_layer=settings.layer_debug + 2)
                    self._debug_sprites.append(worm_sprite)

            self.renderer.add_sprites(self._debug_sprites)
        else:
            # remove debug sprites
            self.renderer.remove_sprites(self._debug_sprites)

    def display_death_message(self):
        message = random.choice(settings.death_messages)

        img = pygametext.getsurf(message, **settings.font_themes["title"])
        spr = Sprite(img, self.cam.position, z_layer=settings.layer_death_message, anchor='midbottom')
        self.renderer.add_sprite(spr)
        def rmv(*args):
            self.renderer.remove_sprite(spr)
        self.tweener.create_tween_by_end(spr, "alpha", 255, 0, settings.hero_reset_interval, do_start=True, cb_end=rmv)


class TilesScrollingBackgroundSprite(Sprite):

    def __init__(self, tile_image, position, z_layer):
        Sprite.__init__(self, tile_image, position, z_layer=z_layer)
        self.draw_special = True
        self._is_initialized = False
        self.tile_image = tile_image
        tw, th = tile_image.get_size()
        self.tw = tw
        self.th = th

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        if not self._is_initialized:
            self.position = cam.position
            iw = cam.rect.width + self.tw
            ih = cam.rect.height + self.th
            img = pygame.Surface((iw, ih))

            self.set_image(img)


logger.debug("imported")
