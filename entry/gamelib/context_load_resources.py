# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_load_resources.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The resource loading context.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ['FakeImage', 'FakeImageLoader', 'FakeImageResource', 'Level', 'ResourceLoaderContext']

import json
import os

import pygame

from gamelib.context_gameplay import EntitySprite
from gamelib.gamelogic.critter import Critter
from gamelib.gamelogic.hero import Hero, EnergyIndicator, GasOMeter
from gamelib.gamelogic.worm import Worm
from pyknic.pyknic_pygame.resource2 import resource_manager, SpriteSheetLoader

from gamelib import settings
from gamelib.gamelogic.rocks import SafeRock, EdibleRock
from gamelib.gamelogic.misc_entities import StartEntity, EndEntity, ClueEntity, JetPack, Herbs
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.resource2 import FakeImage, AbstractResourceLoader, AbstractResource, AbstractResourceConfig, Image, \
    DirectoryAnimation
from pyknic.mathematics import Point3 as Point
from pyknic.pyknic_pygame.spritesystem import TextSprite, Sprite
from pytmxloader.pytmxloader import load_map_from_file_path, TILE_LAYER_TYPE, OBJECT_LAYER_TYPE

logger = logging.getLogger(__name__)
logger.debug("importing...")


class ImageResource(AbstractResource):
    def __init__(self, resource_config, image):
        AbstractResource.__init__(self, resource_config)
        self.image = image


class ImageLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, Image)

    def load(self, resource_config, file_cache):
        key = resource_config.path_to_file
        image = file_cache.get(key, None)
        if image is None:
            image = pygame.image.load(resource_config.path_to_file)
            image = image.convert_alpha()
            file_cache[key] = image
        return ImageResource(resource_config, image)

    def unload(self, res):
        pass


class FakeImageResource(AbstractResource):
    def __init__(self, resource_config, image):
        AbstractResource.__init__(self, resource_config)
        self.image = image


class FakeImageLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, FakeImage)

    def load(self, resource_config, file_cache):
        key = (resource_config.width, resource_config.height, resource_config.fill_color, resource_config.fill_args)
        image = file_cache.get(key, None)
        if image is None:
            image = pygame.Surface((resource_config.width, resource_config.height), flags=pygame.SRCALPHA)
            image.fill(resource_config.fill_color, *resource_config.fill_args)  # todo AFTER_PYWEEK fill_args handling??
            file_cache[key] = image
        return FakeImageResource(resource_config, image)

    def unload(self, res):
        pass


class DirectoryResource(AbstractResource):

    def __init__(self, resource_config):
        AbstractResource.__init__(self, resource_config)
        self.frames = []
        self.fps = resource_config.fps


class DirectoryLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, DirectoryAnimation)

    def load(self, resource_config, file_cache):
        import glob
        pathname = os.path.join(resource_config.path_to_dir, "*.{0}".format(resource_config.extension))
        file_names = glob.glob(pathname)
        file_names.sort()  # todo: does this always sort correctly?
        res = DirectoryResource(resource_config)
        for file_name in file_names:
            img = file_cache.get(file_name, None)
            if img is None:
                img = pygame.image.load(file_name).convert_alpha()
                file_cache[file_name] = img

            if resource_config.transformation:
                # transform and cache transformed image
                t_key = (resource_config.path_to_dir, resource_config.transformation.key)
                t_img = file_cache.get(t_key, None)
                if t_img is None:
                    t_img = resource_config.transformation.transform(img)
                    file_cache[t_key] = t_img  # cache transformed image too
                img = t_img

            res.frames.append(img)
        return res

    def unload(self, res):
        pass


# move this here because the resource loaders should be here
# e.g. FakeImageLoader is the same as the if-block after 'isinstance(n, resource_gfx.FakeImage)' below
# each loader returns its own resource, e.g. FakeImageResource is returned by the FaceImageLoader
# you need to register the loaders in the resource manager, see __init__ in ResourceLoaderContext
def _load_gfx_resources(self):
    pass
    # if Client.img_cache:  # load resources only once
    #     self.images = image_names
    #     return
    # for k, n in image_names.items():
    #     # p = path.join(gfx_path, n)
    #     # img = pygame.image.load(p).convert_alpha()
    #     # img = pygame.transform.rotozoom(img, 0.0, settings.widget_scale)
    #     # self.images[k] = img
    #     self.images[k] = n  # fixme: avoid this by using image_names directly everywhere?
    #
    #     if isinstance(n, resource_gfx.SpriteSheet):
    #         img = Client.img_cache.get(n.path_to_file, None)
    #         if img is None:
    #             img = pygame.image.load(n.path_to_file).convert_alpha()
    #             Client.img_cache[n.path_to_file] = img
    #         n.data = []
    #         r = pygame.Rect((0, 0), n.tile_size)
    #         # iw, ih = img.get_size()
    #         # x_count = iw // r.width  # alternate way to find row and column counts
    #         # y_count = ih // r.height
    #         for y in range(0, n.row_count):
    #             for x in range(0, n.column_count):
    #                 area = r.move(x * r.width, y * r.height)
    #                 s = pygame.Surface(r.size, flags=pygame.SRCALPHA)
    #                 s.blit(img, (0, 0), area=area)
    #                 # todo: cache this images too? which key?
    #                 n.data.append(s)
    #     elif isinstance(n, resource_gfx.Image):
    #         img = Client.img_cache.get(n.path_to_file, None)
    #         if img is None:
    #             img = pygame.image.load(n.path_to_file).convert_alpha()
    #             Client.img_cache[n.path_to_file] = img
    #         n.data = img
    #     elif isinstance(n, resource_gfx.DirectoryAnimation):
    #         import glob
    #         pathname = os.path.join(n.path_to_dir, "*.{0}".format(n.extension))
    #         file_names = glob.glob(pathname)
    #         file_names.sort()  # todo: does this always sort correctly?
    #         n.data = []
    #         for file_name in file_names:
    #             img = Client.img_cache.get(file_name, None)
    #             if img is None:
    #                 img = pygame.image.load(file_name).convert_alpha()
    #                 Client.img_cache[file_name] = img
    #             n.data.append(img)
    #     elif isinstance(n, resource_gfx.FileList):
    #         n.data = []
    #         for name in n.names:
    #             file_name = os.path.join(n.path_to_dir, name)
    #             img = Client.img_cache.get(file_name, None)
    #             if img is None:
    #                 img = pygame.image.load(file_name).convert_alpha()
    #                 Client.img_cache[file_name] = img
    #             n.data.append(img)
    #     elif isinstance(n, resource_gfx.FakeImage):
    #         key = (n.width, n.height, n.fill_color, n.fill_args)
    #         img = Client.img_cache.get(key, None)
    #         if img is None:
    #             img = pygame.Surface((n.width, n.height), flags=pygame.SRCALPHA)
    #             img.fill(n.fill_color)
    #             for a in n.fill_args:
    #                 img.fill(*a)
    #             Client.img_cache[key] = img
    #         n.data = img
    # self.images[settings.KIND_QUEST_ROOM] = self.images[settings.KIND_STAIRWELL_EXIT]
    # image_names[settings.KIND_QUEST_ROOM] = image_names[settings.KIND_STAIRWELL_EXIT]


# kinds:
# anim_hero_jump
# anim_hero_walk
# anim_wave
# anim_worm_eat
# anim_worm_rumble
# anim_goat
# hero
# goat
# worm
# safe_rock
# edible_rock
# clue
# start
# end
# level_boundary
# jetpack

class Level(object):
    """Level holds a Tiled map"""

    def __init__(self):
        self.map_file_name = None
        self.animations = {}  # {anim_id: [animation]}
        self.entities = {}  # {entity: sprite}
        self.sprites = []
        self.image_cache = {}
        self.height = 0
        self.width = 0

    def load_map(self, name, scheduler=None):
        logger.info(">>> LOADING MAP {0}", name)

        self.map_file_name = name
        map_info = load_map_from_file_path(name)
        self.height = map_info.properties['tileheight'] * map_info.properties['height']
        self.width = map_info.properties['tilewidth'] * map_info.properties['width']
        # hack to load the object types properties from objtypes_2.json
        obj_types = self.get_obj_type_mappings()  # {name:kind}

        for idx, layer in enumerate(map_info.layers):
            if not layer.visible:
                continue
            if layer.layer_type == TILE_LAYER_TYPE:
                self._load_tile_layer(idx, self.image_cache, layer, scheduler, self.sprites, self.entities, obj_types)
            elif layer.layer_type == OBJECT_LAYER_TYPE:
                self._load_object_layer(idx, self.image_cache, layer, scheduler, self.sprites, self.entities, obj_types)

        # TODO: anims
        # self._validate_animation_load(settings.ANIM_0_NORMAL)

        logger.info(">>> LOADING MAP {0} DONE", name)
        if None in self.entities:
            del self.entities[None]

    def _load_tile_layer(self, idx, img_cache, layer, scheduler, sprites, entities, obj_types):
        logger.debug("layer {0}", layer.name)

        if settings.debug_skip_bg_tiles and layer.name == 'Ground':
            return

        back_ground_sprites = []
        if layer.name == 'Ground':
            class Composite(Sprite):
                kind = settings.KIND_BACKGROUND_SPRITE

                def __init__(self, rect):
                    Sprite.__init__(self, pygame.Surface(rect.size, pygame.RLEACCEL),
                                    Point(rect.centerx, rect.centery),
                                    z_layer=settings.layer_background)
                    self.rect = rect
                    self.has_sprites = False

                def refresh(self, sprites_in_area):
                    self.image.fill((255, 0, 255))
                    for spr in sprites_in_area:
                        x = int(spr.rect.left - self.rect.left)
                        y = int(spr.rect.top - self.rect.top)
                        self.image.blit(spr.image, (x, y))
                    self.set_image(self.image)
                    self.has_sprites = len(sprites_in_area) > 0

            image = None
            for tile_y in range(layer.height):
                logger.debug("line {0}", tile_y)
                for tile_x in range(layer.width):
                    tile = layer.data_yx[tile_y][tile_x]
                    if tile is None:
                        continue
                    tile_info = tile.tile_info

                    kind = settings.KIND_BACKGROUND_SPRITE

                    if tile_info:
                        image = self._get_cached_image(self.image_cache, tile_info)
                        # if image is None:
                        #     logger.error('missing image ' + str(kind))
                        # else:
                        # name = (
                        # tile_info.tileset.image_path_rel_to_cwd, tile_info.spritesheet_x, tile_info.spritesheet_y)
                        # spr = self.create_sprite(None, idx, image, pos, 0, name)
                        # spr.rect = rect
                        #
                        # if kind == settings.KIND_BACKGROUND_SPRITE:
                        #     back_ground_sprites.append(spr)
                        # else:
                        #     spr.kind = kind
                        #     self.sprites.append(spr)

                    break
                if image:
                    break
            aw = settings.screen_width // 2
            ah = settings.screen_height // 2

            surf = pygame.Surface((aw, ah), pygame.RLEACCEL)
            iw, ih = image.get_size()
            for x in range(0, aw, iw):
                for y in range(0, ah, ih):
                    surf.blit(image, (x, y))

            composite_areas = []

            for x in range(0, self.width, aw):
                for y in range(0, self.height, ah):
                    # composite_areas.append(Composite(pygame.Rect(x, y, aw, ah)))
                    sprites.append(Sprite(surf, Point(x + aw // 2, y + ah // 2)))

            # for area in composite_areas:
            #     pygame.event.pump()
            #     idx_list = area.rect.collidelistall(back_ground_sprites)
            #     area.refresh([back_ground_sprites[idx] for idx in idx_list])
            #
            # sprites.extend([c for c in composite_areas if c.has_sprites])

            return
        else:

            for tile_y in range(layer.height):
                logger.debug("line {0}", tile_y)
                for tile_x in range(layer.width):
                    pygame.event.pump()  # pump the pygame event queue to avoid freezing the window

                    tile = layer.data_yx[tile_y][tile_x]
                    if tile is None:
                        continue
                    pos = Point(tile.tile_x + tile.offset_x, tile.tile_y + tile.offset_y)
                    ti = tile.tile_info

                    kind = ti.properties.get("kind", None) or obj_types.get(ti.type, None)

                    if kind is None:
                        kind = settings.KIND_BACKGROUND_SPRITE

                    rect = pygame.Rect(pos.x, pos.y, layer.width, layer.height)

                    self._create_entity(kind, pos, layer.width, layer.height, 0, ti, idx, img_cache, rect,
                                        back_ground_sprites=back_ground_sprites)

    def get_obj_type_mappings(self):
        objtypes = {}
        with open(os.path.join(settings.map_dir, 'objecttypes_2.json'), 'rb') as f:
            data = json.load(f)
            for d in data:
                name = d['name']
                props = d['properties']
                for p in props:
                    propname = p['name']
                    if propname == 'kind':
                        value = p['value']
                        objtypes[name] = value
                        logger.debug('>>> found type mapping: {}: {}', name, value)
        return objtypes

    def _load_object_layer(self, idx, img_cache, layer, scheduler, sprites, entities, obj_types):
        logger.debug("layer {0}", layer.name)

        # for ot in layer.tiles:
        #     ti = ot.tile_info
        #     kind = ti.properties.get("kind", None) or obj_types.get(ot.type, None) or ot.properties.get("kind", None)
        #
        #     rect = pygame.Rect(ot.pixel_rect)
        #     pos = Point(rect.centerx, rect.centery)
        #
        #     self._create_entity(kind, pos, ot.width, ot.height, ot.rotation, ti, idx, img_cache, rect)

        for obj in layer.objects:
            prop = obj.properties
            kind = prop.get("kind", None) or obj_types.get(obj.type, None)
            tile_info = getattr(obj, "tile_info", None)
            if kind is None and tile_info is not None:
                kind = obj.tile_info.properties.get("kind", None)
            logger.debug('>>> object layer: object kind={}', kind)

            rect = pygame.Rect(obj.pixel_rect)
            pos = Point(rect.centerx, rect.centery)

            self._create_entity(kind, pos, obj.width, obj.height, obj.rotation, tile_info, idx, img_cache, rect, obj)

    def _create_entity(self, kind, pos, width, height, rotation, tile_info, z_layer, image_cache, rect, obj=None,
                       back_ground_sprites=None):
        pos.x += 1500
        pos.y += 1500
        ent = None
        if kind == "anim_hero_jump":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_HERO_JUMP] = frames
            return
        elif kind == "anim_hero_walk":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_HERO_WALK] = frames
            return
        elif kind == "anim_wave":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_WAVE] = frames
            return
        elif kind == "anim_worm_eat":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_WORM_EAT] = frames
            return
        elif kind == "anim_worm_rumble":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_WORM_RUMBLE] = frames
            return
        elif kind == "anim_goat":
            frames = self._load_animation_sprite(image_cache, tile_info)
            self.animations[settings.KIND_ANIM_GOAT] = frames
            return
        elif kind == "hero":
            energy_indicator = EnergyIndicator(settings.no_energy_params)  # todo change for initial params
            gas_o_meter = GasOMeter(settings.no_fart_params)  # todo change for initial params
            ent = Hero(pos, settings.hero_params, energy_indicator, gas_o_meter)
            self.entities[ent] = None
            return
        elif kind == "goat":
            ent = Critter(pos, settings.critter_params, None)
            self.entities[ent] = None
            return
        elif kind == "worm":
            ent = Worm(pos, settings.worm_params, None)
            self.entities[ent] = None
            return
        elif kind == "safe_rock":
            logger.debug('>>> SafeRock tile object')
            rock_params = settings.RockParams(width=width, height=height, rotation=rotation)
            ent = SafeRock(pos, rock_params)
        elif kind == "edible_rock":  # todo why do these rock wiggle when slowly scrolling?
            logger.debug('>>> EdibleRock tile object')
            rock_params = settings.RockParams(width=width, height=height, rotation=rotation)
            ent = EdibleRock(pos, rock_params)
        elif kind == "clue":
            logger.debug('>>> CLUE object')

            clue_id = obj.properties.get('id', None)
            if clue_id is None:
                logger.error('ignoring Clue that has no "id" property at %s', pos.as_xy_tuple())
                return
            clue_params = settings.ClueParams(*rect, clue_id)
            # trigger = event_trigger.CollisionTrigger(pos, rect.w // 2)
            ent = ClueEntity(pos, clue_params)
            ent.rect = rect.copy()
            # ent.trigger = trigger
        elif kind == "start":
            logger.debug('>>> START object')

            start_params = settings.StartParams(pos.x, pos.y, rect.w, rect.h)
            # trigger = event_trigger.CollisionTrigger(pos, rect.w // 2)
            ent = StartEntity(pos, start_params)
            ent.rect = rect.copy()
            # ent.trigger = trigger
        elif kind == "end":
            logger.debug('>>> END object')

            end_params = settings.EndParams(*rect)
            # trigger = event_trigger.CollisionTrigger(pos, rect.w // 2)
            ent = EndEntity(pos, end_params)
            ent.rect = rect.copy()
            # ent.trigger = trigger

        elif kind == "level_boundary":
            pass
        elif kind == "herbs":
            strength = int(tile_info.properties.get("strength", 0))
            ent = Herbs(pos, settings.flatulence[strength])
        elif kind == "jetpack":
            ent = JetPack(pos)
        elif kind == settings.KIND_BACKGROUND_SPRITE:
            pass
        else:
            logger.error("unknown kind (kind, pos, width, height, rotation, z_layer, rect): %s",
                         (kind, pos.as_xy_tuple(), width, height, rotation, z_layer, rect))

        spr = None

        if tile_info:
            image = self._get_cached_image(image_cache, tile_info)
            if image is None:
                logger.error('missing image ' + str(kind))
            else:
                name = (tile_info.tileset.image_path_rel_to_cwd, tile_info.spritesheet_x, tile_info.spritesheet_y)
                spr = self.create_sprite(ent, z_layer, image, pos, rotation, name)
                spr.rect = rect

                if kind == settings.KIND_BACKGROUND_SPRITE:
                    back_ground_sprites.append(spr)
                else:
                    spr.kind = kind if ent is None else ent.kind
                    self.sprites.append(spr)
        self.entities[ent] = spr

    def create_sprite(self, ent, z_layer, image, pos, rotation, name):
        spr = EntitySprite(ent, image, pos, anchor="center", z_layer=z_layer, name=name)
        spr.rotation = -rotation
        # spr.use_sub_pixel = True
        return spr

    # # TODO: create entity from rectangle
    # for ot in layer.rectangles:
    #     kind = ot.properties.get("kind", None)
    #     r = pygame.Rect(ot.x, ot.y, ot.width, ot.height)
    #     spr = None
    #
    #     if kind == "start":
    #         pass
    #     else:
    #         raise Exception("unexpected kind={} in object layer rectangles layer={} name='{}' id={}".format(
    #             kind, layer.name, ot.name, ot.id))
    #
    #     if spr is not None:
    #         text = ot.properties.get("text", None)
    #         x, y, w, h = ot.pixel_rect
    #         text_pos = spr.position + Vec(0, h / 2 + 5)
    #         self._create_text_sprite(sprites, text, text_pos)

    # for ot in layer.poly_lines:
    #     kind = ot.properties.get("kind", None)
    #     ant_path = []
    #     for p in ot.pixel_points:
    #         if kind == "path":
    #             ant_path.append(Node(Point(*p)))
    #         else:
    #             raise Exception("unexpected kind={} in object layer poly lines layer={} name='{}' id={}".format(
    #                 kind, layer.name, ot.name, ot.id))
    #     if ant_path:
    #         logger.debug('ant_path loaded: {}', ant_path)
    #         self.ant_paths.append(ant_path)

    @staticmethod
    def _create_text_sprite(sprites, text, text_pos):
        if text is not None:
            ts = TextSprite(text, text_pos, z_layer=99, anchor="midtop")
            ts.name = str(id(ts))
            sprites.append(ts)

    @staticmethod
    def _get_cached_image(img_cache, ti):
        image_path = ti.tileset.image_path_rel_to_cwd
        key = (image_path, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
        img = img_cache.get(key, None)
        if img is None:
            spritesheet = img_cache.get(image_path, None)
            if spritesheet is None:
                # noinspection PyUnresolvedReferences
                logger.info("loading image: " + str(image_path))
                spritesheet = pygame.image.load(image_path).convert_alpha()
                img_cache[image_path] = spritesheet
            area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
            img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
            img.blit(spritesheet, (0, 0), area)
            img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
            if ti.angle != 0:
                img = pygame.transform.rotate(img, ti.angle)
            # ip = ti.tileset.image_path_rel_to_cwd
            # if "space1_0_4x_smooth.png" in ip:
            #     img = img.convert()

            # if PIXEL_PPU != 1.0:  # todo should we implement ppu once again?
            #     if PIXEL_PPU == 2.0:
            #         img = pygame.transform.scale2x(img)
            #     else:
            #         _w, _h = img.get_size()
            #         img = pygame.transform.smoothscale(img, (int(_w * PIXEL_PPU), int(_h * PIXEL_PPU)))

            img = img.convert_alpha()
            img_cache[key] = img
        return img

    def _load_animation_sprite(self, img_cache, ti):
        frames = []
        fps = 1.0
        for af in ti.animation:
            img = self._get_cached_image(img_cache, af.tile_info)
            fps = 1.0 / (af.duration / 1000.0)
            frames.append(img)
        return frames, fps
    #
    # def _load_special_animation_sprite(self, layer, img_cache, position, ti, scheduler, flip_y=False):
    #     frames = []
    #     fps = 1.0
    #     for af in ti.animation:
    #         img = self._get_cached_image(img_cache, af.tile_info)
    #         if flip_y:
    #             img = pygame.transform.flip(img, False, True)
    #         fps = 1.0 / (af.duration / 1000.0)
    #         frames.append(img)
    #     spr = SpecialAnimation(position, LAYER_HERO, frames, fps, scheduler)
    #     return spr
    #
    # def _validate_animation_load(self, anim_id):
    #     if self.animations.get(anim_id, None) is None:
    #         raise Exception("missing anim id " + anim_id)
    #     if len(self.animations[anim_id]) < 2:
    #         raise Exception("no/less animations loaded (anim id {0}) should at leas be 3 but {1}".format(anim_id, len(
    #             self.animations[anim_id])))


class MapResource(AbstractResource):
    def __init__(self, resource_config, level, sprites, entities):
        AbstractResource.__init__(self, resource_config)
        self.entities = entities
        self.sprites = sprites
        self.level = level


class MapResourceConfig(AbstractResourceConfig):

    def __init__(self, path_to_map):
        AbstractResourceConfig.__init__(self)
        self.path_to_map = path_to_map


class MapLoader(AbstractResourceLoader):  # todo AFTER_PYWEEK: redesign resource loader for nested resources

    def __init__(self):
        AbstractResourceLoader.__init__(self, MapResourceConfig)

    def load(self, resource_config, file_cache):
        level = Level()
        level.load_map(resource_config.path_to_map)
        return MapResource(resource_config, level, level.sprites, level.entities)

    def unload(self, res):
        pass


class ResourceLoaderContext(TimeSteppedContext):

    def __init__(self, resource_def):
        """
        Loads the resources in a own context...
        :param resource_def: the resources to load
        """
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.resource_def = resource_def
        resource_manager.register_loader(SpriteSheetLoader(), True)
        resource_manager.register_loader(FakeImageLoader(), True)
        resource_manager.register_loader(MapLoader(), True)
        resource_manager.register_loader(ImageLoader(), True)
        resource_manager.register_loader(DirectoryLoader(), True)
        self.screen = None
        self._loaded = False

    def enter(self):
        logger.info("clearing resources")
        resource_manager.resources.clear()
        self.screen = pygame.display.get_surface()

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        pass

    def update_step(self, delta, sim_time, *args):
        if not self._loaded:  # execute only once, in rare occasions this is called multiple times!
            logger.info("loading resources")
            resource_manager.load_resources(self.resource_def, self.progress_cb)
            self.pop()
            self._loaded = True

    def progress_cb(self, *args):
        idx, total, percent, res_id, res = args
        logger.info(f"RESOURCE PROGRESS: {idx}/{total}, {percent}%, res_id {res_id}, res {res}")
        if self.screen:
            self.screen.fill((0, 0, 0))
            r_w = settings.screen_width
            r_h = 50
            r = pygame.Rect(0, 0, int(r_w * percent), r_h)
            r.bottomleft = (0, settings.screen_height - 100)
            pygame.draw.rect(self.screen, (0, 180, 0), r)
            r.width = r_w
            # r.midbottom = (settings.screen_width // 2, settings.screen_height - 100)
            pygame.draw.rect(self.screen, (180, 180, 180), r, 1)
            pygame.display.flip()


logger.debug("imported")
