# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'critter.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Critter"]  # list of public visible parts of this module

import random

from gamelib import settings
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.settings import EVT_NOISE, EVT_GOAT_STATE_CHANGE, EVT_KILLED
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState
from pyknic.mathematics import Vec3

logger = logging.getLogger(__name__)
logger.debug("importing...")


class CritterBaseState(BaseState):

    @staticmethod
    def elapsed(owner, *args):
        raise NotImplementedError()


class CritterStand(CritterBaseState):

    @staticmethod
    def enter(owner):
        interval = random.random() * (
                owner.params.stand_max_duration - owner.params.stand_min_duration) + owner.params.stand_min_duration
        owner.schedule_id = owner.scheduler.schedule(owner.elapsed, interval)

    @staticmethod
    def exit(owner):
        owner.scheduler.remove(owner.schedule_id)

    @staticmethod
    def elapsed(owner, *args):
        if random.random() < owner.params.stand_turn_chance:  # todo critter params!
            owner.state_machine.switch_state(CritterTurn)
        else:
            owner.state_machine.switch_state(CritterWalk)
        return 0


class CritterTurn(CritterBaseState):

    @staticmethod
    def enter(owner):
        interval = random.random() * (
                owner.params.turn_max_duration - owner.params.turn_min_duration) + owner.params.turn_min_duration
        owner.schedule_id = owner.scheduler.schedule(owner.elapsed, interval)
        diff = owner.center - owner.position
        d = diff.length + 30
        dist = random.randint(10, 250) / 100 * d
        diff.rotate(random.randint(-80, 80))
        diff.length = dist
        owner.target.copy_values(owner.position + diff)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)
        if abs(owner.direction.angle - diff.angle) < 2:
            if random.random() < owner.params.turn_stand_chance:
                owner.state_machine.switch_state(CritterStand)
            else:
                owner.state_machine.switch_state(CritterWalk)

    @staticmethod
    def exit(owner):
        owner.scheduler.remove(owner.schedule_id)

    @staticmethod
    def elapsed(owner, *args):
        if random.random() < owner.params.turn_stand_chance:
            owner.state_machine.switch_state(CritterStand)
        else:
            owner.state_machine.switch_state(CritterWalk)
        return 0


class CritterWalk(CritterBaseState):

    @staticmethod
    def enter(owner):
        owner.schedule_id = owner.scheduler.schedule(owner.elapsed, owner.params.noise_interval)

    @staticmethod
    def exit(owner):
        owner.scheduler.remove(owner.schedule_id)

    @staticmethod
    def elapsed(owner, *args):
        event_dispatcher.fire(EVT_NOISE, owner)
        return owner.params.noise_interval

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)
        owner.position += owner.direction * owner.params.speed * dt

        if diff.length_sq < 15 * 15:
            if random.random() < 0.5:
                owner.state_machine.switch_state(CritterStand)
            else:
                owner.state_machine.switch_state(CritterTurn)


class CritterDead(CritterBaseState):

    @staticmethod
    def enter(owner):
        event_dispatcher.fire(EVT_KILLED, owner)

    @staticmethod
    def elapsed(owner, *args):
        return 0


class Critter(StateDrivenAgentBase):
    kind = settings.KIND_GOAT

    def __init__(self, position, critter_params, scheduler):
        StateDrivenAgentBase.__init__(self, CritterStand, self)
        self.params = critter_params
        self.position = position
        self.direction = Vec3(1, 0)
        self.target = position.clone()
        self.center = position.clone()
        self.scheduler = scheduler
        self.schedule_id = None
        self.state_machine.event_switched_state += self._log_state_change

    def elapsed(self, *args):
        return self.state_machine.current_state.elapsed(self, *args)

    def die(self):
        self.state_machine.switch_state(CritterDead)

    def _log_state_change(self, *args):
        event_dispatcher.fire_instant(EVT_GOAT_STATE_CHANGE, self)
        owner, prev, sm = args
        logger.info(
            f"critter({id(self)}) state change: {prev.__name__} -> {self.state_machine.current_state.__name__}")

    def is_alive(self):
        return self.state_machine.current_state != CritterDead


logger.debug("imported")
