# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_load_resources.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""event_trigger.py - an object that can be activated by satisfying its requirements

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []


# TODO: ?
# class RequirementAlreadyExists(Exception):
#     message = 'duplicate reqid found'


class Requirement(object):
    def __init__(self, reqid, text=''):
        self.reqid = reqid
        self.text = text
        self.satisfied = False


class BaseTrigger(object):
    def __init__(self, requirements=None):
        """an object that can be activated by satisfying its requirements

        :param requirements: sequence of Requirement
        """
        self.requirements = {}
        if requirements is not None:
            for r in requirements:
                self.add_requirement(r)
        self.active = True

    def set_inactive(self):
        self.active = False

    def is_active(self):
        """If trigger is not active, it can be ignored.

        There is no magic here. It is just a convenience to allow management of a state that can be checked manually.
        If you plan to use this, then it's up to you to manually check and honor it.

        This is needed for use-once scenarios. Once a trigger has been satisfied, and the dependent game logic has been
        completed, call trigger.set_inactive(). You may also destroy the trigger (remove/unregister it wherever it has
        been saved).
        """
        return self.active

    def add_requirement(self, requirement):
        """Add a Requirement"""
        self.requirements[requirement.reqid] = requirement

    def is_satisfied(self, reqid):
        """Is Requirement satisfied

        Return boolean
        """
        return self.requirements[reqid].satisfied is True

    def is_all_satisfied(self):
        """Are all Requirements satisfied

        Return boolean
        """
        return all(r.satisfied for r in self.requirements.values())

    def is_needed(self, reqid):
        """Is Requirement needed

        Return boolean
        """
        return self.requirements[reqid].satisfied is False

    def is_any_needed(self):
        """Is any Requirement needed

        Return boolean
        """
        return any(r.satisfied is False for r in self.requirements.values())

    def needs_what(self):
        """What Requirements are needed

        Return generator, reqid from Requirements that are still needed
        """
        return tuple(r.reqid for r in self.requirements.values() if r.satisfied is False)

    def get_requirement(self, reqid):
        """Get Requirement by reqid

        Return Requirement
        """
        return self.requirements[reqid]

    def get_requirements(self):
        """Get all Requirements

        Return generator, all Requirements
        """
        return self.requirements.values()

    def satisfy(self, reqid):
        """Set the Requirement.satisfied to True"""
        self.requirements[reqid].satisfied = True

    def satisfy_all(self):
        """Set all Requirement.satisfied to True"""
        for r in self.requirements.values():
            r.satisfied = True


class LogicTrigger(BaseTrigger):
    pass


class CollisionTrigger(BaseTrigger):
    def __init__(self, pos, radius, requirements=None):
        BaseTrigger.__init__(self, requirements)
        self.pos = pos
        self.radius = radius


if __name__ == '__main__':

    def check_init(t, r):
        """initial state of a trigger and a requirement should be..."""
        assert t.get_requirement(r.reqid) is r
        assert r in t.get_requirements()
        #
        assert t.is_satisfied(r.reqid) is False
        assert t.is_needed(r.reqid) is True
        #
        assert t.is_any_needed() is True
        assert r.reqid in tuple(t.needs_what())

    def check_satisfied(t, r):
        """satisfied state of a requirement should be..."""
        assert t.get_requirement(r.reqid) is r
        assert r in t.get_requirements()
        #
        assert t.is_satisfied(r.reqid) is True
        assert t.is_needed(r.reqid) is False
        #
        assert r.reqid not in tuple(t.needs_what())

    def check_not_satisfied(t, r):
        """unsatisfied state of a requirement should be..."""
        assert t.get_requirement(r.reqid) is r
        assert r in t.get_requirements()
        #
        assert t.is_satisfied(r.reqid) is False
        assert t.is_needed(r.reqid) is True
        #
        assert r.reqid in tuple(t.needs_what())

    def check_all(t, expected):
        """are all requirements satisfied?"""
        assert t.is_all_satisfied() is expected
        assert t.is_any_needed() is not expected


    # test collision requirement

    # assumes positive collision test and no requirements...
    t = CollisionTrigger((0, 0), 1)
    check_all(t, True)

    # test 1 requirement

    r1 = Requirement(1, 'shoo goat')
    t = BaseTrigger([r1])
    check_init(t, r1)
    check_all(t, False)
    #
    t.satisfy(1)
    check_satisfied(t, r1)
    check_all(t, True)

    # test 2 requirements

    r1 = Requirement(1, 'shoo goat')
    r2 = Requirement(2, 'get jetpack')
    t = BaseTrigger([r1, r2])
    check_init(t, r1)
    check_init(t, r2)
    check_all(t, False)
    #
    t.satisfy(1)
    check_satisfied(t, r1) and check_not_satisfied(t, r2)
    check_all(t, False)
    #
    t.satisfy(2)
    check_satisfied(t, r1) and check_satisfied(t, r2)
    check_all(t, True)
