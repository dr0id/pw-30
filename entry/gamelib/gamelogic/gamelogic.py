# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gamelogic.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The main file of the game logic.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameLogic"]  # list of public visible parts of this module

from gamelib import settings
from gamelib.gamelogic import scripts, story
from gamelib.gamelogic.sfx_handler import SfxHandler
from gamelib.gamelogic.collider import Collider
from gamelib.gamelogic.critter import CritterStand, CritterDead
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.gamelogic.hero import HeroWalkState, HeroStand
from gamelib.gamerender import spritefx, game_texts
from gamelib.settings import KIND_WORM, KIND_SAFE_ROCK
from pyknic.timing import Scheduler, Timer

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GameLogic(object):

    def __init__(self):
        self.level = 1
        self.hero = None
        self.last_safe_position = None
        self.scheduler = Scheduler()

        self.entities = []

        self._noisy_entities = []  # used internally to pass noise information to the worms, decouple calls

        self.collider = Collider()
        self.collider.register(KIND_WORM, KIND_SAFE_ROCK, self._collide_worm_safe_rock)
        self.collider.register(settings.KIND_HERO, settings.KIND_START, self._collide_hero_vs_start)
        self.collider.register(settings.KIND_HERO, settings.KIND_END, self._collide_hero_vs_end)
        self.collider.register(settings.KIND_HERO, settings.KIND_CLUE, self._collide_hero_vs_clue)
        self.collider.register(settings.KIND_HERO, settings.KIND_JETPACK, self._collide_hero_vs_jetpack)
        self.collider.register(settings.KIND_HERO, settings.KIND_HERBS, self._collide_hero_vs_herbs)
        self.collider.register(settings.KIND_HERO, settings.KIND_SAFE_ROCK, self._collide_hero_vs_saferock)

        event_dispatcher.register_event_type(settings.EVT_KILLED)
        event_dispatcher.register_event_type(settings.EVT_RESCUE_GOAT)
        event_dispatcher.add_listener(settings.EVT_KILLED, self.killed_entity)
        event_dispatcher.add_listener(settings.EVT_RESCUE_GOAT, self._rescue_goat)

        self.sfx_handler = SfxHandler(settings.music_player)
        self.sfx_handler.register_events(event_dispatcher)
        event_dispatcher.fire(settings.EVT_MUSIC_STARTED, settings.EVT_MUSIC_STARTED)

        self.next_clue_entity = None
        self._seen_clue_ids = []
        self.current_clue_idx = 0
        self.clues_order = []

    def initialize_entities(self, entities):
        self.clues_order = []
        start = None
        end = None
        for ent in entities:
            if ent.kind == settings.KIND_WORM:
                ent.set_timer(Timer(self.scheduler))
            elif ent.kind == settings.KIND_GOAT:
                ent.scheduler = self.scheduler
                ent.state_machine.switch_state(CritterStand)
            elif ent.kind == settings.KIND_HERO:
                self.hero = ent
                self.last_safe_position = self.hero.position.clone()
            elif ent.kind == settings.KIND_START:  # there can be only one...
                self.next_clue_entity = ent
                start = ent
            elif ent.kind == settings.KIND_CLUE:
                self.clues_order.append(ent)
            elif ent.kind == settings.KIND_END:
                end = ent

            self.entities.append(ent)  # todo add entities to the spacebin?
        self.clues_order.sort(key=lambda c: c.id)
        self.clues_order.append(end)
        self.clues_order.insert(0, start)
        self.current_clue_idx = 0
        self.next_clue_entity = self.clues_order[self.current_clue_idx]

    def start_opening_credits(self, *args):
        if settings.debug_skip_intro is True:
            "ya loser :)"
            settings.milestones.append(settings.GAME_TITLE_PLAYED)
        else:
            game_texts.GameTitleBanner()

    def _reset_hero(self, *args):
        self.hero.revive(self.last_safe_position)
        return 0  # unschedule from scheduler

    def killed_entity(self, ent, *args):
        logger.info("KILLED %s", ent.__class__.__name__)
        if ent == self.hero:
            logger.info("DEAD")
            self.scheduler.schedule(self._reset_hero, settings.hero_reset_interval)
            # todo death message!
        else:
            if ent in self.entities:  # multiple worms can have targeted the same prey..
                self.entities.remove(ent)

    def _collide_hero_vs_saferock(self, heros, rocks):
        for r in rocks:
            for h in heros:
                if h.position.get_distance_sq(r.position) < (h.radius + r.radius) ** 2:
                    self.last_safe_position = r.position.clone()
                    break

    def _rescue_goat(self, *args):
        goats = list(e for e in self.entities if e.kind == settings.KIND_GOAT and e.state_machine.current_state != CritterDead)
        if goats:
            g = goats[0]
            g.state_machine.switch_state(CritterStand)
            g.position.x = self.hero.position.x + 50
            g.position.y = self.hero.position.y + 25

    def _collide_hero_vs_herbs(self, heros, herbs):
        # for h in heros:
        #     for herb in herbs:
        #         if not h.state_machine.current_state in (HeroWalkState, HeroStand):
        #             return
        #         if h.position.get_distance_sq(herb.position) < (h.radius + herb.radius)**2:
        #
        #             h.gas_o_meter.params = herb.params
        #
        #             if herb in self.entities:
        #                 self.entities.remove(herb)
        #             event_dispatcher.fire_instant(settings.EVT_ENTITY_REMOVED, herb)
        pass

    def _collide_hero_vs_jetpack(self, heros, packs):
        # for h in heros:
        #     for p in packs:
        #         if h.position.get_distance_sq(p.position) < (h.radius + p.radius)**2:
        #             h.energy_indicator.params = settings.energy_params
        #             if p in self.entities:
        #                 self.entities.remove(p)
        #             event_dispatcher.fire_instant(settings.EVT_ENTITY_REMOVED, p)
        pass

    def _collide_worm_safe_rock(self, worms, rocks):
        for worm in worms:
            for rock in rocks:
                if worm.position.get_distance_sq(rock.position) < (worm.radius + rock.radius) ** 2:
                    worm.collide_with_rock(rock)

    def _collide_hero_vs_start(self, heroes, starts):
        for hero in heroes:
            if hero.state_machine.current_state not in (HeroWalkState, HeroStand):
                return
            for start in starts:
                if hero.position.get_distance_sq(start.position) < (hero.radius + start.radius) ** 2:
                    # logger.debug('!!!! collision: hero vs start')
                    message = story.start_messages[self.level]
                    if story.meets_requirements(message):
                        if not story.was_seen(message):
                            story.Story(message)
                            event_dispatcher.fire(settings.EVT_CLUE_FOUND)
                            self.update_blink_entity(start)

    def _collide_hero_vs_end(self, heroes, ends):
        for hero in heroes:
            if hero.state_machine.current_state not in (HeroWalkState, HeroStand):
                return
            for end in ends:
                if hero.position.get_distance_sq(end.position) < (hero.radius + end.radius) ** 2:
                    # logger.debug('!!!! collision: hero vs end')
                    message = story.end_messages[self.level]
                    if story.meets_requirements(message):
                        if not story.was_seen(message):
                            story.Story(message)
                            event_dispatcher.fire(settings.EVT_CLUE_FOUND)
                            self.update_blink_entity(end)

    def _story_triggers_object(self, clue_id):
        h = self.hero
        if clue_id == '1.2':
            h.energy_indicator.params = settings.energy_params
            for p in list([e for e in self.entities if e.kind == settings.KIND_JETPACK]):
                self.entities.remove(p)
                event_dispatcher.fire_instant(settings.EVT_ENTITY_REMOVED, p)
        elif clue_id == '1.3':
            herbs = list(e for e in self.entities if e.kind == settings.KIND_HERBS)
            if herbs:
                herb = herbs[0]
                h.gas_o_meter.params = herb.params

    def _collide_hero_vs_clue(self, heroes, clues):
        for hero in heroes:
            if hero.state_machine.current_state not in (HeroWalkState, HeroStand):
                return
            for clue in clues:
                if hero.position.get_distance_sq(clue.position) < (hero.radius + clue.radius) ** 2:
                    # logger.debug('!!!! collision: hero vs clue')
                    message = story.clue_messages.get(clue.id, None)
                    if message is None:
                        logger.error('No clue text for id={}', clue.id)
                    else:
                        if story.meets_requirements(message):
                            if not story.was_seen(message):
                                story.Story(message)
                                self._story_triggers_object(clue.id)
                                event_dispatcher.fire(settings.EVT_CLUE_FOUND)
                                self.update_blink_entity(clue)

    def update_blink_entity(self, ent):
        try:
            if ent not in self._seen_clue_ids:
                if ent in self.clues_order:
                    if ent == self.clues_order[self.current_clue_idx]:
                        self.current_clue_idx += 1
                        if self.current_clue_idx == len(self.clues_order):
                            pass  # no next entity
                        else:
                            self._seen_clue_ids.append(ent)
                            self.next_clue_entity = self.clues_order[self.current_clue_idx]
                    else:
                        pass  # not next entity
                else:
                    pass  # not in list
        except:
            # This happens when you reach the EndEntity.
            # File "\pw-30\entry\gamelib\gamelogic\gamelogic.py", line 247, in update_blink_entity
            #   if ent == self.clues_order[self.current_clue_idx]:
            # IndexError('list index out of range')
            pass

        # next_id = story.get_next_clue_id(clue_id)
        # if next_id and next_id not in self._seen_clue_ids:
        #     clues = [_e for _e in self.entities if _e.kind == settings.KIND_CLUE and _e.id == next_id]
        #     if len(clues) > 0:
        #         self._seen_clue_ids.append(next_id)
        #         next_clue = clues[0]
        #         self.next_clue_entity = next_clue
        #     else:
        #         logger.error("could not find clue entity for next clue id %s", next_id)
        # else:
        #     ends = [_e for _e in self.entities if _e.kind == settings.KIND_END]
        #     if len(ends)> 0:
        #         self.next_clue_entity = ends[0]

    def update(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time, *args)

        entities_in_region = self.entities  # todo fill in the entities in the region of the hero....

        for e in (_e for _e in entities_in_region if
                  _e.kind in (settings.KIND_HERO, settings.KIND_GOAT, settings.KIND_WORM)):
            e.update(delta, sim_time, *args)

        self.collider.check_all_collisions(entities_in_region)
        event_dispatcher.update_all()

        spritefx.update_tweens(delta)
        scripts.update(delta)
        story.update(delta)

    def handle_actions(self, actions):
        for action, extra in actions:
            # hero movement
            if action == settings.ACTION_HERO_MOVE_UP:
                self.hero.key_state[0] = 1
            elif action == settings.ACTION_HERO_STOP_UP:
                self.hero.key_state[0] = 0
            elif action == settings.ACTION_HERO_MOVE_RIGHT:
                self.hero.key_state[1] = 1
            elif action == settings.ACTION_HERO_STOP_RIGHT:
                self.hero.key_state[1] = 0
            elif action == settings.ACTION_HERO_MOVE_DOWN:
                self.hero.key_state[2] = 1
            elif action == settings.ACTION_HERO_STOP_DOWN:
                self.hero.key_state[2] = 0
            elif action == settings.ACTION_HERO_MOVE_LEFT:
                self.hero.key_state[3] = 1
            elif action == settings.ACTION_HERO_STOP_LEFT:
                self.hero.key_state[3] = 0

            # jump
            elif action == settings.ACTION_HERO_FIRE:
                self.hero.jump(extra)
            elif action == settings.ACTION_HERO_TRIGGER_DISCHARGE:
                self.hero.trigger_discharge()
            elif action == settings.ACTION_HERO_AIM:
                pos, rel, buttons = extra
                self.hero.update_direction(pos)

    def suspend(self):
        if self.hero:
            self.hero.clear_key_state()

    def exit(self):
        self.hero.clear_key_state()


logger.debug("imported")
