# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'hero.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The hero entity.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

from gamelib import settings
from gamelib.settings import EVT_HERO_JUMPED, EVT_HERO_FARTED, EVT_KILLED
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.settings import KIND_HERO, air_steering_is_relative, EVT_NOISE, EVT_HERO_STATE_CHANGE
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState
from pyknic.mathematics import Vec3 as Vec, Point3 as Point

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Hero", "EnergyIndicator"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def _check_accidental_discharge(owner, go_straight=False):
    if owner.gas_o_meter.should_discharge():
        return _triggered_discharge(go_straight, owner)
    return None


def _triggered_discharge(go_straight, owner):
    amount = owner.gas_o_meter.trigger_discharge()
    if go_straight:
        diff = owner.velocity.normalized * amount * owner.energy_indicator.max_energy
        diff.z = 0
        return diff
    else:
        v = owner.direction.clone()
        v.length = amount * owner.energy_indicator.max_energy
        v.rotate(random.randint(-owner.fart_angle_inaccuracy_in_deg, owner.fart_angle_inaccuracy_in_deg))
        return v


class HeroBaseState(BaseState):

    @staticmethod
    def jump(owner, target):
        pass

    @staticmethod
    def trigger_discharge(owner):
        pass

    @staticmethod
    def update_direction(owner, pos):
        pass


class HeroWalkState(HeroBaseState):
    @staticmethod
    def enter(owner):
        owner.velocity.z = 0
        event_dispatcher.fire(EVT_NOISE, owner)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.energy_indicator.update(dt, sim_t, *args)
        owner.gas_o_meter.update(dt, sim_t, *args)

        owner.velocity.x = owner.key_state[1] - owner.key_state[3]
        owner.velocity.y = owner.key_state[2] - owner.key_state[0]
        owner.velocity.length = owner.speed
        owner.position += owner.velocity * dt

        if owner.velocity.x != 0 or owner.velocity.y != 0:
            event_dispatcher.fire(EVT_NOISE, owner)
        else:
            owner.state_machine.switch_state(HeroStand)
            return

        # update target
        diff = owner.mouse_position - owner.position
        dist = diff.length
        radius = dist if dist < owner.energy_indicator.current_energy else owner.energy_indicator.current_energy
        owner.target.copy_values(owner.position + owner.direction * radius)

        v = _check_accidental_discharge(owner)
        if v:
            HeroWalkState.jump(owner, owner.position + v, False)

    @staticmethod
    def jump(owner, target, do_use_energy=True):
        if do_use_energy is True and owner.energy_indicator.params.max_energy == 0:
            return

        if do_use_energy is False and owner.gas_o_meter.params == settings.no_fart_params:
            return

        difference = target - owner.position
        magnitude_sq = difference.length_sq
        used_energy = difference.length
        current_energy = owner.energy_indicator.current_energy
        if magnitude_sq > current_energy ** 2:
            # target is further away as the energy allows
            difference.length = current_energy
            used_energy = current_energy
        if do_use_energy:
            owner.energy_indicator.remove(used_energy)
        owner.target.copy_values(owner.position + difference)

        owner.state_machine.switch_state(HeroInAir)

    @staticmethod
    def trigger_discharge(owner):
        p = _triggered_discharge(False, owner)
        HeroWalkState.jump(owner, owner.position + p, False)

    @staticmethod
    def update_direction(owner, pos):
        owner.direction = (pos - owner.position).normalized


class HeroStand(HeroWalkState):

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.energy_indicator.update(dt, sim_t, *args)
        owner.gas_o_meter.update(dt, sim_t, *args)

        owner.velocity.x = owner.key_state[1] - owner.key_state[3]
        owner.velocity.y = owner.key_state[2] - owner.key_state[0]
        owner.velocity.length = owner.speed
        owner.position += owner.velocity * dt

        if owner.velocity.x != 0 or owner.velocity.y != 0:
            owner.state_machine.switch_state(HeroWalkState)
            return

        # update target
        diff = owner.mouse_position - owner.position
        dist = diff.length
        radius = dist if dist < owner.energy_indicator.current_energy else owner.energy_indicator.current_energy
        owner.target.copy_values(owner.position + owner.direction * radius)

        v = _check_accidental_discharge(owner)
        if v:
            HeroWalkState.jump(owner, owner.position + v, False)


def _steer_in_air(owner, dt):
    if air_steering_is_relative:
        dx = owner.key_state[1] - owner.key_state[3]
        perp = owner.velocity.normal_left
        perp.length = dx * owner.params.air_steering_speed
        ds = perp * dt

        owner.position += ds
        owner.target += ds
    else:
        dx = owner.key_state[1] - owner.key_state[3]
        dy = owner.key_state[2] - owner.key_state[0]
        perp = owner.velocity.normal_left
        perp.length = owner.params.air_steering_speed
        ds = perp * dt
        dx = dx if owner.velocity.y < 0 else -dx
        dy = dy if owner.velocity.x >= 0 else -dy

        owner.position.x += dx * ds.x
        owner.position.y += dy * ds.y
        owner.target.x += dx * ds.x
        owner.target.y += dy * ds.y


class HeroInAir(HeroBaseState):

    @staticmethod
    def enter(owner):
        # based on http://www.mathforgameprogrammers.com/gdc2016/GDC2016_Pittman_Kyle_BuildingABetterJump.pdf
        diff = owner.target - owner.position
        half_dist = diff.length / 2.0
        v = owner.jump_speed

        owner.velocity = diff.normalized * v

        h = owner.jump_height
        if half_dist < 10:
            th = 1.5
            owner.velocity.z = (2 * h) / th  # v0
            owner.acc.z = (-2 * h) / (th * th)  # acc
        else:
            owner.velocity.z = (2 * h * v) / half_dist  # v0
            owner.acc.z = (-2 * h * v * v) / (half_dist * half_dist)  # acc

    @staticmethod
    def exit(owner):
        owner.acc.z = 0
        owner.velocity = Vec(0, 0, 0)
        owner.position.z = 0

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # owner.energy_indicator.update(dt, sim_t, *args)
        owner.gas_o_meter.update(dt, sim_t, *args)

        owner.position += owner.velocity * dt
        owner.velocity += owner.acc * dt

        _steer_in_air(owner, dt)

        v = _check_accidental_discharge(owner, True)
        if v:
            owner.target.copy_values(owner.position + v)
            owner.state_machine.switch_state(HeroDoubleJump)
            return
        if owner.position.z <= 0:
            owner.state_machine.switch_state(HeroWalkState)

    @staticmethod
    def trigger_discharge(owner):
        amount = owner.gas_o_meter.trigger_discharge()
        diff = owner.velocity.normalized * amount * owner.energy_indicator.max_energy
        diff.z = 0
        owner.target.copy_values(owner.target + diff)
        owner.state_machine.switch_state(HeroDoubleJump)


class HeroDoubleJump(HeroBaseState):

    @staticmethod
    def enter(owner):
        # based on http://www.mathforgameprogrammers.com/gdc2016/GDC2016_Pittman_Kyle_BuildingABetterJump.pdf
        diff = owner.target - owner.position
        half_dist = diff.length / 2.0
        v = owner.jump_speed

        owner.velocity = diff.normalized * v
        angle_change = random.randint(-owner.fart_angle_inaccuracy_in_deg, owner.fart_angle_inaccuracy_in_deg)
        owner.velocity.rotate(angle_change)
        diff.rotate(angle_change)
        owner.target.copy_values(owner.position + diff)

        h = owner.jump_height
        if half_dist < 10:
            th = (owner.discharge_th_s * owner.gas_o_meter.last_amount) + 1  # seconds
            owner.velocity.z = (2 * h) / th  # v0
            owner.acc.z = (-2 * h) / (th * th)  # acc
        else:
            owner.velocity.z = (2 * h * v) / half_dist  # v0
            owner.acc.z = (-2 * h * v * v) / (half_dist * half_dist)  # acc

    @staticmethod
    def exit(owner):
        owner.acc.z = 0
        owner.velocity = Vec(0, 0, 0)
        owner.position.z = 0

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # owner.energy_indicator.update(dt, sim_t, *args)
        owner.gas_o_meter.update(dt, sim_t, *args)

        owner.position += owner.velocity * dt
        owner.velocity += owner.acc * dt

        _steer_in_air(owner, dt)

        if owner.position.z <= 0:
            owner.state_machine.switch_state(HeroWalkState)


class HeroDead(HeroBaseState):
    @staticmethod
    def enter(owner):
        event_dispatcher.fire(EVT_KILLED, owner)


class Hero(StateDrivenAgentBase):
    kind = KIND_HERO

    def __init__(self, position, hero_params, energy_indicator, gas_o_meter):
        StateDrivenAgentBase.__init__(self, HeroWalkState, self)
        self.gas_o_meter = gas_o_meter
        self.energy_indicator = energy_indicator
        self.position = position
        self.velocity = Vec(0, 0, 0)
        self.key_state = [0] * 4
        self.speed = hero_params.speed
        self.target = self.position.clone()
        self.jump_speed = hero_params.jump_speed
        self.jump_height = hero_params.jump_height
        self.fart_angle_inaccuracy_in_deg = hero_params.fart_angle_inaccuracy_in_deg
        self.discharge_th_s = hero_params.discharge_th_s
        self.direction = Vec(1, 0, 0)
        self.params = hero_params
        self.mouse_position = Point(0, 0)
        self.radius = hero_params.radius

        self.vz0 = 0
        self.acc = Vec(0, 0, 0)
        self.state_machine.event_switched_state += self._log_state_change

    def _log_state_change(self, *args):
        event_dispatcher.fire_instant(EVT_HERO_STATE_CHANGE, self)
        owner, prev, sm = args
        logger.info(
            f"hero state change: {prev.__name__} -> {self.state_machine.current_state.__name__}")

    def clear_key_state(self):
        self.key_state = [0] * 4

    def jump(self, target):
        if settings.CLUE_JETPACK in settings.milestones:
            self.state_machine.current_state.jump(self, target)
            event_dispatcher.fire(EVT_HERO_JUMPED, self)

    def trigger_discharge(self):
        self.state_machine.current_state.trigger_discharge(self)

    def update_direction(self, pos):
        self.mouse_position = pos
        self.state_machine.current_state.update_direction(self, pos)

    def die(self):
        self.state_machine.switch_state(HeroDead)

    def revive(self, position):
        self.position.copy_values(position)
        self.position.z = 0
        self.state_machine.switch_state(HeroStand)

    def is_alive(self):
        return self.state_machine.current_state != HeroDead


class EnergyIndicator(object):

    def __init__(self, energy_params):
        self.params = energy_params
        self.current_energy = self.params.max_energy
        self.last_used_energy = 0

    def update(self, dt, sim_time, *args):
        if self.current_energy < self.params.max_energy:
            self.current_energy += self.params.regeneration_rate * dt
            if self.current_energy >= self.params.max_energy:
                self.current_energy = self.params.max_energy
            # logger.debug("energy updated: " + str(self.current_energy))

    @property
    def max_energy(self):
        return self.params.max_energy

    def remove(self, value):
        self.current_energy -= value
        self.last_used_energy = value
        self.current_energy = 0 if self.current_energy < 0 else self.current_energy
        logger.info("energy removed: " + str(value) + " left: " + str(self.current_energy))


class GasOMeter(object):
    """
     __________________________________
    |     build up          | critical |
    |_______________________|__________|
    0 -->      1               2       3

    1 build up of gas
    2 critical, there is a chance it might go off
    3 explode!

    """

    def __init__(self, fart_params):
        self.current_level = 0
        self.params = fart_params
        self.last_amount = 0

    # noinspection PyUnusedLocal
    def update(self, dt, sim_time, *args):
        if self.current_level < self.params.max_level:
            self.current_level += self.params.generation_rate * dt
            self.current_level = self.params.max_level if self.current_level >= self.params.max_level else self.current_level
            # logger.debug(f"gas at: {self.current_level}")

    def should_discharge(self):
        if self.current_level >= self.params.max_level:
            return True
        elif self.current_level >= self.params.critical_level:
            if random.random() <= self.params.explode_chance:
                return True
            return False
        else:
            return False

    def trigger_discharge(self):
        if settings.CLUE_FART_BOOST in settings.milestones:
            current_level = self.current_level
            self.current_level = 0
            logger.info(f"gas discharged: {current_level}")
            self.last_amount = current_level / self.params.max_level * self.params.range_extension
            event_dispatcher.fire(EVT_HERO_FARTED, current_level)
            return self.last_amount
        else:
            return 0


logger.debug("imported")
