# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_load_resources.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The rocks entity.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

from gamelib import settings

logger = logging.getLogger(__name__)
logger.debug("importing...")


class StartEntity(object):
    kind = settings.KIND_START

    def __init__(self, position, start_params):
        self.position = position
        self.size = start_params.width, start_params.height
        self.radius = max(self.size) / 2
        # Note: needs a CollisionTrigger object; level logic needs to determine requirements
        self.trigger = None


class EndEntity(object):
    kind = settings.KIND_END

    def __init__(self, position, end_params):
        self.position = position
        self.size = end_params.width, end_params.height
        self.radius = max(self.size) / 2
        # Note: needs a CollisionTrigger object; level logic needs to determine requirements
        self.trigger = None


class ClueEntity(object):
    kind = settings.KIND_CLUE

    def __init__(self, position, clue_params):
        self.id = clue_params.id
        self.position = position
        self.size = clue_params.width, clue_params.height
        self.radius = max(self.size) / 2
        # Note: needs a CollisionTrigger object; level logic needs to determine requirements
        self.trigger = None


class JetPack(object):
    kind = settings.KIND_JETPACK

    def __init__(self, pos):
        self.position = pos
        self.radius = 10

class Herbs(object):
    kind = settings.KIND_HERBS

    def __init__(self, pos, params):
        self.position = pos
        self.params = params
        self.radius = 20

logger.debug("imported")
