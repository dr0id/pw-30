# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resource_sfx.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os

from gamelib import settings
from pyknic.generators import IdGenerator
from pyknic.pyknic_pygame.sfx import SoundData, MusicData

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

logger.error(os.path.abspath(settings.__file__))

_gen_id = IdGenerator(1)

SFX_FART_1 = _gen_id.next()
SFX_FART_2 = _gen_id.next()
SFX_FART_3 = _gen_id.next()
SFX_FART_4 = _gen_id.next()
SFX_FART_5 = _gen_id.next()
SFX_FART_6 = _gen_id.next()
SFX_FART_7 = _gen_id.next()
SFX_GOAT_1 = _gen_id.next()
SFX_GOAT_2 = _gen_id.next()
SFX_GOAT_3 = _gen_id.next()
SFX_JETPACK = _gen_id.next()
SFX_RUMBLE = _gen_id.next()
SFX_FOUND = _gen_id.next()

sfx_farts = (
    SFX_FART_1,
    SFX_FART_2,
    SFX_FART_3,
    SFX_FART_4,
    SFX_FART_5,
    SFX_FART_6,
    SFX_FART_7,
)

sfx_goats = (
    SFX_GOAT_1,
    SFX_GOAT_2,
    SFX_GOAT_3,
)

sfx_data = {
    SFX_FART_1: SoundData(os.path.join('data/sounds/fart', 'Ascending-Fart-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_2: SoundData(os.path.join('data/sounds/fart', 'Deep-Fart-A2-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_3: SoundData(os.path.join('data/sounds/fart', 'Deflating-Fart-A2-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_4: SoundData(os.path.join('data/sounds/fart', 'Long-Fart-A1-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_5: SoundData(os.path.join('data/sounds/fart', 'Short-and-funny-fart-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_6: SoundData(os.path.join('data/sounds/fart', 'Singing-Fart-A1-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FART_7: SoundData(os.path.join('data/sounds/fart', 'To-The-Moon-Fart-www.fesliyanstudios.com.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_GOAT_1: SoundData(os.path.join('data/sounds/goat', 'goat1.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_GOAT_2: SoundData(os.path.join('data/sounds/goat', 'goat2.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_GOAT_3: SoundData(os.path.join('data/sounds/goat', 'goat3.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_JETPACK: SoundData(os.path.join('data/sounds/jetpack', 'jetpack.ogg'),
                           settings.sfx_volume * settings.master_volume),
    SFX_RUMBLE: SoundData(os.path.join('data/sounds/rumble', 'powerful_rumble_loop.ogg'),
                          settings.sfx_volume * settings.master_volume),
    SFX_FOUND: SoundData(os.path.join('data/sounds/misc', '145434__soughtaftersounds__old-music-box-1.ogg'),
                         settings.sfx_volume * settings.master_volume),
}

songs = [
    MusicData(os.path.join('data', 'music', 'Crypto.ogg'), 0.4),
]

#songtimes = [115.0, 18.3333, 23.3333, 55.0]

logger.debug("imported")
