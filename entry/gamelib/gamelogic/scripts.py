# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from pyknic.generators import IdGenerator

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def update(dt):
    if scripts:
        s = scripts[0]
        s.start()
        s.update(dt)
        if s.state == STATE_ENDED:
            scripts.remove(s)


scripts = []

_gen = IdGenerator(0)
STATE_INIT = _gen.next()
STATE_DELAY = _gen.next()
STATE_RUNNING = _gen.next()
STATE_PAUSED = _gen.next()
STATE_ENDED = _gen.next()

state_names = {
    STATE_INIT: 'STATE_INIT',
    STATE_DELAY: 'STATE_DELAY',
    STATE_RUNNING: 'STATE_RUNNING',
    STATE_PAUSED: 'STATE_PAUSED',
    STATE_ENDED: 'STATE_ENDED',
}


class TextScript(object):
    def __init__(self, sentences, delay=0, min_duration=4, words_per_sec=2, callback=None):
        """metered text emitter with callback

        :param sentences: sequence of strings
        :param delay: seconds to wait before posting first sentence
        :param words_per_sec: tick one second for each N words (duration measured by words)
        :param min_duration: minimum duration (for very short sentences)
        :param callback: call func(text, TextScript_object) when a new text is posted
        """
        if sentences:
            logger.debug('TextScript: new script: first text={}', sentences[0])
        else:
            logger.warning('TextScript: new script: empty text')
        self.sentences = sentences
        self.delay = delay
        self.callback = callback
        self.progress = -1
        self.words_per_sec = words_per_sec
        self.min_duration = float(min_duration)
        self.elapsed = 0
        self.current_text = None
        self.duration = 0
        self.state = STATE_INIT
        scripts.append(self)

    def start(self):
        if self.state == STATE_INIT:
            self._advance_progress()

    def pause(self):
        if self.state == STATE_RUNNING:
            self._change_state(STATE_PAUSED)

    def resume(self):
        if self.state == STATE_PAUSED:
            self._change_state(STATE_RUNNING)

    def update(self, dt):
        if self.state == STATE_DELAY:
            self.elapsed += dt
            if self.elapsed >= self.delay:
                self._advance_progress()
        elif self.state == STATE_RUNNING:
            self.elapsed += dt
            if self.elapsed > self.duration:
                self._advance_progress()

    def _change_state(self, new_state):
        logger.debug('TextScript: state changed {} -> {}', state_names[self.state], state_names[new_state])
        self.state = new_state

    def _advance_progress(self):
        if self.state == STATE_INIT:
            if self.delay > 0:
                self._change_state(STATE_DELAY)
            else:
                self._next_text()
                self._change_state(STATE_RUNNING)
        elif self.state == STATE_DELAY:
            self._next_text()
            self._change_state(STATE_RUNNING)
        elif self.state == STATE_RUNNING:
            if self.progress >= len(self.sentences) - 1:
                self._change_state(STATE_ENDED)
                # if self.callback:
                #     self.callback(self.current_text, self)
                self.current_text = None
            else:
                self._next_text()

    def _next_text(self):
        self.progress += 1
        self.elapsed = 0
        self.current_text = self.sentences[self.progress]
        num_words = len(self.current_text.split())
        self.duration = max(self.min_duration, 0.5 + float(num_words) / self.words_per_sec)
        self._post_text()

    def _post_text(self):
        if not self.current_text:
            return
        if self.callback:
            self.callback(self.current_text, self)

    def get_text(self):
        return self.current_text


if __name__ == '__main__':
    sentences = (
        'The quick brown fox',
        'jumped over the lazy dog\'s back',
        '123456789 times.',
    )
    tps = 3.
    dt = 1. / tps

    def test1():
        t = TextScript(sentences, delay=2)
        clock = pygame.time.Clock()
        elapsed = 0
        running = True
        while running:
            clock.tick(tps)
            elapsed += dt
            if elapsed > 3 and t.state == STATE_INIT:
                print('Starting')
                t.start()
            t.update(dt)
            print('{:.02f} {:.02f} {} {}'.format(elapsed, t.elapsed, state_names[t.state], t.current_text))
            if t.state == STATE_ENDED:
                running = not running

    def test2(quiet=False):
        def emit(text, obj):
            print('emit: ' + text)

        sentences = (
            'This is a longer sentence to force the calculation.',
            'If the words are more than you can read in min_duration,',
            'then the words_per_sec calculation comes into effect.',
        )
        t = TextScript(sentences, callback=emit)
        needed = True
        clock = pygame.time.Clock()
        running = True
        while running:
            clock.tick(tps)
            if needed:
                t.start()
                needed = not needed
            t.update(dt)
            if t.state == STATE_ENDED:
                running = not running
            if not quiet:
                print('{:.02f} {} {}'.format(t.elapsed, state_names[t.state], t.current_text))

    # test1()
    test2(quiet=True)


logger.debug("imported")
