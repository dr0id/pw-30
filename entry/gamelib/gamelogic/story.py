# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

from gamelib import settings
from gamelib.gamelogic import scripts
from gamelib.gamelogic.eventdispatcher import event_dispatcher
# from gamelib.gamelogic.event_trigger import BaseTrigger, Requirement
from gamelib.gamerender import game_texts
from pyknic.generators import IdGenerator

logger = logging.getLogger(__name__)
logger.debug("importing...")


def update(dt):
    if stories:
        s = stories[0]
        if s.state == STATE_ENDED:
            stories.remove(s)
        else:
            s.start()
            # s.update(dt)
            if s.script.state == scripts.STATE_ENDED:
                s._change_state(STATE_ENDED)


stories = []


_gen = IdGenerator(0)
STATE_INIT = _gen.next()
STATE_RUNNING = _gen.next()
STATE_ENDED = _gen.next()

state_names = {
    STATE_INIT: 'STATE_INIT',
    STATE_RUNNING: 'STATE_RUNNING',
    STATE_ENDED: 'STATE_ENDED',
}



# NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE
#
# If a string in the 'text' list begins with a '{', it is parsed for an embedded command.
# An embedded command is case-sensitive, and must be in the form: "{command:value}".
#
# Example embedded command:
#   start_messages = {
#       1: {
#           ...
#           'text': [
#               "{sfx:fart}Ah, what a relief.",
#           ]

# spawn point objects per level
start_messages = {
    1: {  # clue unlocks intro
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Wha...what happened?",
            "The ship. Crash landing!",
            "I'm a castaway...",
            "No! Grr-rr, a Survivor.",
            "Maybe there are others.",
            "...",
            "Let's have a look around these rocks.",
        ],
        'requires': [settings.GAME_TITLE_PLAYED],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_INTRO],
    },
    2: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 2",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
    3: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 3",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
    4: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 4",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
    5: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 5",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
    6: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 6",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
    7: {
        'kind': settings.KIND_START,
        'seen': False,
        'delay': 3,
        'text': [
            "Start 7",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [],
    },
}

end_messages = {
    1: {  # clue unlocks level 2
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Okay, I'm here!",
            "Can we please go now!?",
            "...",
            "They are probably in cryo-sleep.",
            "Wouldn't that be funny.",
            "Harr.",
        ],
        'requires': [settings.CLUE_PETROGLYPH],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_2],  # todo: triggering this, hero farts to load the next level
    },
    2: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Exit 2",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_3],  # todo: triggering this, hero farts to load the next level
    },
    3: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Exit 3",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_4],
    },
    4: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 20,
        'text': [
            "Exit 4",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_5],
    },
    5: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Exit 5",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_6],
    },
    6: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Exit 6",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_LEVEL_7],
    },
    7: {
        'kind': settings.KIND_END,
        'seen': False,
        'delay': 3,
        'text': [
            "Exit 7",
        ],
        'requires': [],
        'start_unlocks': [],
        'end_unlocks': [settings.STORY_GAME_WON],
    },
}


def get_next_clue_id(clue_id):
    clue_ids = list(sorted(clue_messages.keys()))
    if clue_id not in clue_ids:
        return None
    clue_idx = clue_ids.index(clue_id)
    if clue_idx == len(clue_ids):
        return None
    return clue_ids[clue_idx+1]


clue_messages = {
    '1.1': {  # clue triggers goat munch
        'kind': settings.KIND_CLUE,
        'seen': False,
        'delay': 3,
        'text': [
            "Trekking. Climbing. <gasp> I'll starve.",
            "No critters. No weeds. Sand forever.",
            "What in tarnation can survive here?",
            "{spawn:goat}Hey, there's a...!",
            "{sfx:goat}A goat?",
            "Hey, buddy. I bet you're called Billy. Heeere Billy-Billy...",
        ],
        'requires': [settings.CLUE_INTRO],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_GOAT],
    },
    '1.2': {  # jetpack unlocks jumping skill
        'kind': settings.KIND_CLUE,
        'seen': False,
        'delay': 3,
        'text': [
            "What a place. If they're here...",
            "They can't have gone far.",
            "What's this? A JETPACK!",
            "{sfx:jetpack}It works. Strange, someone just left it.",
            "I always say, \"Never let a good jetpack go to waste.\" Har-har.",
        ],
        'requires': [settings.CLUE_GOAT],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_JETPACK],
    },
    '1.3': {  # local herb unlocks farting skill
        'kind': settings.KIND_CLUE,
        'seen': False,
        'delay': 3,
        'text': [
            "Ooo...is this edible?",
            "What's the rule? Spiny leaves are bad?",
            "No, spiny leaves are good?",
            "Ah, rats, I forget. Well, here goes nothing.",
            "{sfx:fart}...",
            "Woah! That did NOT come out of my...",
            "Nah, couldn't be. I only had one bite. Bon appetit!"
        ],
        'requires': [settings.CLUE_JETPACK],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_FART_BOOST],
    },
    # "A campsite. Of course, no fire.",
    # "Stuff's laid out. Dusty. Must have left in a hurry. Long ago.",
    # "Packages. Hmph. Can't read these markings.",
    # "Tough. Grr. Opening. And...",
    # "Smells food-like. Thank you. Oh, happy day!",
    # "And dehydrated water. Someone is looking out for me today.",
    '1.4': {  # clue unlocks diary
        'kind': settings.KIND_CLUE,
        'seen': False,
        'delay': 3,
        'text': [
            "Someone dropped a book.",
            "English! A diary. Let's skip to the end...",
            '"Dear Diary, Another furnace of a day, like yesterday."',
            '"And the day before. And the day before."',
            '"I used to love the beach."',
            '"I am the last of my party. If anyone finds this..."',
            '"GET OFF THIS HELL OF A PLANET NOW!!!!"',
            'And a mark running fast off the page. As if... the pen were snatched away.',
            'Ok, then. let me guess the ending.',
            '"Aaa-Aa-Aa-A-AAAaaa!!"'
        ],
        'requires': [settings.CLUE_FART_BOOST],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_DIARY],
    },
    '1.5': {  # clue unlocks petroglyph
        'kind': settings.KIND_CLUE,
        'seen': False,
        'delay': 3,
        'text': [
            "Hmm. Caveman markings.",
            '"Nyah, nyah, Neanderthal. Can\'t make fire, can\'t make spear."',
            "Har-har. What ancient genius wrote that? Supremely funny.",
            "Looks like a man. In a spacesuit. Long, long, arching line...",
            "Northeast?",
            "Hrrm, okay. Checklist: jetpack, check; food, check; water, check.",
            "Fart booster, check. Har-har-har! Harr.",
            "Harr.",
        ],
        'requires': [settings.CLUE_DIARY],
        'start_unlocks': [],
        'end_unlocks': [settings.CLUE_PETROGLYPH],
    },
}


def meets_requirements(message):
    for r in message['requires']:
        if r not in settings.milestones:
            return False
    return True


def was_seen(message):
    return message['seen']


def clue_is_unlocked(clue_id):
    return clue_messages[clue_id]['requires'] in settings.milestones


def end_is_unlocked(level):
    return end_messages[level]['requires'] in settings.milestones


class Story(object):
    def __init__(self, story_message):
        # story_message is:
        #  story.start_messages[level]
        #  story.end_messages[level]
        #  story.clues[clue_id]
        self.story_message = story_message
        story_message['seen'] = True

        assert 'text' in story_message
        assert 'delay' in story_message
        assert 'seen' in story_message
        assert 'requires' in story_message
        assert 'start_unlocks' in story_message
        assert 'end_unlocks' in story_message
        assert len(story_message['text']) > 0
        self.texts = story_message['text']
        self.delay = story_message['delay']
        self.requires = story_message['requires']
        self.start_unlocks = story_message['start_unlocks']
        self.end_unlocks = story_message['end_unlocks']

        self.state = STATE_INIT
        self.story_kind = story_message.get('kind', 'unknown_kind')
        self.script = None
        self.current_text = None
        stories.append(self)

        logger.debug('Story queued: kind={} text[0]={[0]}', self.story_kind, self.texts[0])

    def start(self):
        if self.state != STATE_INIT:
            return
        logger.debug('Story: starting: {}', self.texts[0])
        self.script = scripts.TextScript(self.texts, delay=self.delay, callback=self._script_callback)
        self.script.start()
        self._change_state(STATE_RUNNING)
        # todo: fire an event?
        settings.milestones.extend(self.story_message['start_unlocks'])
        logger.debug('Story: start: unlocked={}', self.story_message['end_unlocks'])

    def update(self, dt):
        pass

    def _change_state(self, new_state):
        logger.debug('Story: state changed {} -> {}', state_names[self.state], state_names[new_state])
        self.state = new_state

    def _script_callback(self, text, obj):
        logger.debug('Story: Script callback: id={} text={}', id(self), text)
        if text.startswith('{'):
            try:
                # check for embedded command
                embedded, text = text.split('}')
                logger.debug('Story: found embedded command: {}', embedded)
                embedded = embedded.strip('{}')
                command, value = embedded.split(':')
                logger.debug('Story: parsed command={} value={}', command, value)
                if command == 'sfx':
                    if value == 'fart':
                        logger.debug('Story: farting!')
                        event_dispatcher.fire(settings.EVT_HERO_FARTED)
                    elif value == 'goat':
                        logger.debug('Story: farting!')
                        event_dispatcher.fire(settings.EVT_GOAT_BAAH)
                    elif value == 'jetpack':
                        logger.debug('Story: farting!')
                        event_dispatcher.fire(settings.EVT_HERO_JUMPED)
                elif command == 'spawn':
                    if value == 'goat':
                        event_dispatcher.fire(settings.EVT_RESCUE_GOAT)
            except:
                logger.error('Story: non-fatal: embedded command failed: text={}', text)
        self.current_text = text
        game_texts.SpeechBubble(text, obj.duration, cb_end=self._bubble_end)

    def _bubble_end(self, sprite, attr_name, tween):
        logger.debug('Story: SpriteFX callback: kind={}, text={}', self.story_kind, self.current_text)
        sprite.tween_callback_end(sprite, attr_name, tween)
        if self.current_text == self.texts[-1]:
            self._change_state(STATE_ENDED)
            # todo: fire an event?
            settings.milestones.extend(self.story_message['end_unlocks'])
            logger.debug('Story: end: unlocked={}', self.story_message['end_unlocks'])


logger.debug("imported")
