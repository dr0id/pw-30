# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'worm.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The worm entity.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

from gamelib import settings
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.settings import KIND_WORM, EVT_KILLED, EVT_WORM_STATE_CHANGE
from pyknic.ai.statemachines import BaseState, StateDrivenAgentBase
from pyknic.mathematics import Vec3 as Vec

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Worm"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class WormBaseState(BaseState):

    @staticmethod
    def register_movement_from(owner, prey):
        pass

    @staticmethod
    def timer_elapsed(owner):
        pass

    @staticmethod
    def collide_with_rock(owner, rock):
        to_target = owner.target - owner.position
        to_rock = rock.position - owner.position
        da = 1 if to_target.cross(to_rock).z < 0 else -1
        dx = (owner.radius + rock.radius) - to_rock.length
        to_rock.length = dx * 1.1
        owner.position -= to_rock

        owner.direction.rotate(da * 30)


class WormWanderState(WormBaseState):

    @staticmethod
    def enter(owner):
        interval = random.random() * owner.params.wander_range_interval + owner.params.wander_min_interval
        owner.timer.start(interval, repeat=True)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def timer_elapsed(owner):
        diff = owner.target - owner.position
        diff.length = 150
        change_angle = random.randint(-owner.params.wander_direction_angle_change,
                                      owner.params.wander_direction_angle_change)
        diff.rotate(change_angle)
        owner.target.copy_values(owner.position + diff)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)
        owner.position += owner.params.wander_speed * owner.direction * dt
        if random.random() < owner.params.wander_submerge_chance:
            owner.state_machine.switch_state(WormWanderSubmergedState)

    @staticmethod
    def register_movement_from(owner, prey):
        owner.target.copy_values(prey.position)
        owner.prey = prey
        owner.state_machine.switch_state(WormSeekState)


class WormWanderSubmergedState(WormWanderState):

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        WormWanderState.update(owner, dt, sim_t, *args)
        if random.random() < owner.params.wander_surface_chance:
            owner.state_machine.switch_state(WormWanderState)


class WormSeekState(WormBaseState):

    @staticmethod
    def enter(owner):
        interval = random.random() * owner.params.give_up_min_interval + owner.params.give_up_max_interval
        owner.timer.start(interval, repeat=True)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def timer_elapsed(owner):
        # give up
        owner.state_machine.switch_state(WormWanderState)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)

        owner.position += owner.params.seek_speed * owner.direction * dt

        if diff.length_sq < owner.params.seek_dip_dist_sq:
            owner.state_machine.switch_state(WormDipState)

    @staticmethod
    def register_movement_from(owner, prey):
        wight = 2 # this will just copy the values if not overwritten
        if owner.prey is not None:
            d1 = owner.position.get_distance(owner.prey.position)
            wight = d1 / (owner.position.get_distance(prey.position) + d1)
        if random.random() < wight:
            owner.timer.stop()
            owner.timer.start()
            owner.target.copy_values(prey.position)
            owner.prey = prey

class WormDipState(WormBaseState):
    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)

        owner.position += owner.params.seek_speed * owner.direction * dt

        # todo check if prey is on safe ground or not!
        if diff.length_sq < owner.params.dip_attack_dist_sq:
            owner.state_machine.switch_state(WormAttackState)

    @staticmethod
    def enter(owner):
        interval = random.random() * owner.params.give_up_min_interval + owner.params.give_up_max_interval
        owner.timer.start(interval, repeat=True)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def timer_elapsed(owner):
        # give up
        owner.state_machine.switch_state(WormWanderState)

    @staticmethod
    def register_movement_from(owner, prey):
        owner.target.copy_values(prey.position)
        owner.prey = prey
        owner.direction = (owner.target - owner.position).normalized


class WormAttackState(WormBaseState):
    @staticmethod
    def enter(owner):
        interval = owner.params.attack_duration
        owner.timer.start(interval)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def timer_elapsed(owner):
        owner.state_machine.switch_state(WormKillState)

    @staticmethod
    def register_movement_from(owner, prey):
        owner.target.copy_values(prey.position)
        owner.prey = prey
        owner.direction = (owner.target - owner.position).normalized

    @staticmethod
    def collide_with_rock(owner, rock):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        diff = owner.target - owner.position
        ad = 1 if owner.direction.cross(diff).z > 0 else -1
        owner.direction.rotate(ad * owner.params.turn_rate * dt)

        owner.position += owner.params.seek_speed * owner.direction * dt


class WormKillState(WormBaseState):

    @staticmethod
    def enter(owner):
        interval = owner.params.kill_duration
        owner.timer.start(interval)
        if owner.prey:
            dist_sq = owner.position.get_distance_sq(owner.prey.position)
            if dist_sq < owner.radius * owner.radius:
                if owner.prey.is_alive():
                    owner.prey.die()
                owner.prey = None

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def timer_elapsed(owner):
        owner.state_machine.switch_state(WormWanderState)

    @staticmethod
    def register_movement_from(owner, prey):
        owner.target.copy_values(prey.position)
        owner.prey = prey
        owner.direction = (owner.target - owner.position).normalized

    @staticmethod
    def collide_with_rock(owner, rock):
        pass


class Worm(StateDrivenAgentBase):
    kind = KIND_WORM

    def __init__(self, position, worm_params, timer):
        StateDrivenAgentBase.__init__(self, WormWanderState, self)
        self.radius = worm_params.radius
        self.position = position
        self.direction = Vec(1, 0)
        self.prey = None
        self.timer = timer
        self.params = worm_params
        self.target = self.position.clone()
        self.state_machine.event_switched_state += self._log_state_change
        event_dispatcher.add_listener(settings.EVT_NOISE, self.register_movement_at)


    def set_timer(self, timer):  # todo ensure this ia called in gameLogic
        self.timer = timer
        self.timer.event_elapsed += self._tick
        interval = random.random() * self.params.wander_range_interval + self.params.wander_min_interval
        self.timer.start(interval, repeat=True)

    def _log_state_change(self, *args):
        event_dispatcher.fire_instant(EVT_WORM_STATE_CHANGE, self)
        owner, prev, sm = args
        logger.info(
            f"{self.timer._name} worm state change: {prev.__name__} -> {self.state_machine.current_state.__name__}")

    def register_movement_at(self, prey):
        if self.position.get_distance_sq(prey.position) < self.params.max_hear_distance_sq:
            self.state_machine.current_state.register_movement_from(self, prey)

    def _tick(self, *args):
        self.state_machine.current_state.timer_elapsed(self)

    def collide_with_rock(self, rock):
        self.state_machine.current_state.collide_with_rock(self, rock)


logger.debug("imported")
