# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

import pygame

from gamelib import settings
from gamelib.gamerender import pygametext, spritefx
from pyknic import tweening

logger = logging.getLogger(__name__)
logger.debug("importing...")


speech_bubbles = []


class GameTitleBanner(object):
    def __init__(self):
        self.text = settings.title
        self.duration = 5
        self.x = settings.screen_width // 2
        self.y = settings.screen_height * 3.5 // 10
        self.a1 = 140
        self.a2 = 0

        self.sprite = spritefx.TextSprite(
            'DR0ID & Gumm Studios presents',
            (self.x, self.y),
            expire=self.duration + 2, theme_name='intro1', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration + 2, tween_function=tweening.ease_in_sine2, cb_end=self._msg_2)

    def _msg_2(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite = spritefx.TextSprite(
            'A DR0ID & Gumm major motion picture',
            (self.x, self.y),
            expire=self.duration + 2, theme_name='intro1', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration + 2, tween_function=tweening.ease_in_sine2, cb_end=self._msg_3)

    def _msg_3(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite = spritefx.TextSprite(
            'Starring',
            (self.x, self.y),
            expire=self.duration - 1, theme_name='intro2', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration - 2, tween_function=tweening.ease_in_sine2, cb_end=self._msg_4)

    def _msg_4(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite = spritefx.TextSprite(
            'Grizzled Space Marine',
            (self.x, self.y),
            expire=self.duration, theme_name='intro2', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration, tween_function=tweening.ease_in_sine2, cb_end=self._msg_5)

    def _msg_5(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite = spritefx.TextSprite(
            'And',
            (self.x, self.y),
            expire=self.duration, theme_name='intro2', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration, tween_function=tweening.ease_in_sine2, cb_end=self._msg_6)

    def _msg_6(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite = spritefx.TextSprite(
            'A Goat',
            (self.x, self.y),
            expire=self.duration, theme_name='intro2', anchor='center')
        self.sprite.fade_effect(self.a1, self.a2, self.duration, tween_function=tweening.ease_in_sine2, cb_end=self._msg_7)

    def _msg_7(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)

        self.sprite = spritefx.TextSprite(self.text, (0, 0), expire=5, theme_name='title')
        width, height = self.sprite.image.get_size()
        self.sprite.driftx_effect(-width, settings.screen_width // 2 - width // 2, 0.75,
                                  tween_function=tweening.ease_in_out_quad, cb_end=self._pause)
        self.sprite.y = settings.screen_height * 3 // 10

    def _pause(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite.misc_effect('my_pause_attr', 0.0, 1.0, 0.75, cb_end=self._fade)

    def _fade(self, sprite, attr_name, tween):
        self.sprite.tween_callback_end(sprite, attr_name, tween)
        self.sprite.fade_effect(end_alpha=32, duration=2.0, tween_function=tweening.ease_in_cubic)
        self.sprite.scale_effect(1.0, 0.25, 2.0, tween_function=tweening.ease_in_cubic)
        self.sprite.drifty_effect(sprite.y, settings.screen_height, 2.0, tween_function=tweening.ease_in_quad)
        self.sprite.driftx_effect(sprite.x, sprite.x + 200, 2.0, cb_end=self._done)

    def _done(self, sprite, attr_name, tween):
        settings.milestones.append(settings.GAME_TITLE_PLAYED)
        sprite.tween_callback_end(sprite, attr_name, tween)


class SpeechBubble(object):
    def __init__(self, text, duration, cb_end=None):
        logger.debug('>>> SpeechBubble: new text={}', text)
        self.text = text
        self.duration = duration
        self.parent_callback = cb_end

        theme = settings.font_themes['speechbubble']
        txt_image = pygametext.getsurf(text, width=200, **theme)

        w, h = txt_image.get_size()
        colorkey = pygame.Color('pink')
        bubble_image = pygame.Surface((w + 20, h + 20))
        bubble_image.fill(colorkey)
        bubble_rect = bubble_image.get_rect()
        txt_rect = txt_image.get_rect(center=bubble_rect.center)
        txt_rect.y -= 3
        frame_rect = bubble_image.get_rect()
        frame_rect.inflate_ip(-4, -7)
        frame_rect.y -= 3
        bubble_image.fill(pygame.Color('white'), frame_rect)
        pygame.draw.rect(bubble_image, pygame.Color('green'), frame_rect, 1)
        x = int(w - w * 0.2)
        y = frame_rect.bottom
        pygame.draw.aaline(bubble_image, pygame.Color('green'), (x - 3, y), (x, y + 3), 1)
        pygame.draw.aaline(bubble_image, pygame.Color('green'), (x, y), (x, y + 3), 1)
        bubble_image.blit(txt_image, txt_rect)
        self.sprite = spritefx.SpriteFX(bubble_image, (500, 350), colorkey=colorkey, anchor='bottomright')
        self.sprite.misc_effect('misc_effect_attr', 0, 1, duration, cb_end=self._cb_end)

        speech_bubbles.append(self)

    def _cb_end(self, sprite, attr_name, tween):
        global current_speech_bubble
        logging.debug('>>> SpeechBubble: end text={}', self.text)
        if self in speech_bubbles:
            speech_bubbles.remove(self)
        if self.parent_callback:
            self.parent_callback(sprite, attr_name, tween)


logger.debug("imported")
