# -*- coding: utf-8 -*-
from __future__ import print_function

import logging
import random

import pygame

from gamelib.gamerender.hudlight import HUD
from gamelib import settings
from gamelib.gamerender import pygametext

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GameHUD(object):
    def __init__(self):
        self.debug_hud = HUD(**settings.font_themes['debug'])
        self.debug_hud.add('hero pos', 'Hero Pos: {}', (0, 0))
        self.debug_hud.add('camera pos', 'Camera Pos: {}', (0, 0))
        self.debug_hud.add('bg tiles', '#sprites: {}', 0)
        self.debug_hud.add('fps', '{:.0f} fps', 0)
        self.debug_hud.add('logic frames', '{:.0f} logic frames', 0)
        self.debug_hud.add('milestone', 'Milestone: {}', '')

        self.update(0, 0, 'INIT')

    def set_hero_pos(self, pos):
        x = int(pos[0])
        y = int(pos[1])
        self.debug_hud.update_item('hero pos', (x, y))

    def set_camera_pos(self, pos):
        x = int(pos[0])
        y = int(pos[1])
        self.debug_hud.update_item('camera pos', (x, y))

    def set_fps(self, fps):
        self.debug_hud.update_item('fps', fps)

    def set_frames(self, fps):
        self.debug_hud.update_item('logic frames', fps)

    def set_bg_tiles(self, n):
        self.debug_hud.update_item('bg tiles', n)

    def set_milestone(self, m):
        self.debug_hud.update_item('milestone', m)

    def update(self, *args):
        pass

    def draw(self, surf, *args, **kwargs):
        self.debug_hud.draw(surf)


logger.debug("imported")
