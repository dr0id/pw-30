# -*- coding: utf-8 -*-
import logging

import pygame

from gamelib import settings
from gamelib.gamelogic import interpolation
from gamelib.gamerender import pygametext
from pyknic import tweening
from pyknic.generators import IdGenerator
from gamelib.gamelogic.vec2d import Vec2d


__version__ = '3.0.1'
__author__ = 'Gummbum, (c) 2017'
__vernum__ = tuple(int(s) for s in __version__.split('.'))
__all__ = ['SpriteFX', 'TextSprite', 'make_floating_score', 'update_tweens', 'clear', 'tweener', 'text_sprites']


logger = logging.getLogger(__name__)
logger.debug("importing...")

tweener = tweening.Tweener()
idgen = IdGenerator(0)
text_sprites = set()


def update_tweens(dt, update_image_cache=True):
    """update the tweens, refresh images, and the image cache
    
    If you aren't using any of the tween effects, you don't need to call this.
    
    This function updates settings.image_cache. If you want to update the image cache elsewhere, then pass in
    update_image_cache=False.
    
    :param dt: time slice from the clock
    :param update_image_cache: update settings.image_cache or not
    :return: None
    """
    tweener.update(dt)
    dirty_sprites = set()
    dirty_sprites_add = dirty_sprites.add
    for t in tweener.get_all_tweens():
        sprite = t.o
        if sprite._dirty:
            dirty_sprites_add(sprite)
    for sprite in dirty_sprites:
        sprite._refresh_image()
    if update_image_cache:
        settings.image_cache.update()


def clear(do_tweens=True, do_texts=True):
    """clear active tweens and/or texts"""
    if do_tweens:
        tweener.clear()
    if do_texts:
        text_sprites.clear()


def make_floating_score(score_text, pos, theme_name='floating_text', expire=1.5, layer=0, colorkey=None):
    """a sample utility to make floating text"""
    ts = TextSprite(score_text, pos, theme_name=theme_name, expire=expire, layer=layer, colorkey=colorkey)
    starty = pos[1] - 100 * settings.time_step
    endy = pos[1] - 10 * settings.time_step
    ts.scale_effect(end_scale=0.5, duration=expire).drifty_effect(start_y=starty, end_y=endy, duration=expire)


id_history = {}


class SpriteFX(object):
    """a sprite class that provides a number of manual and tween-managed effects and interpolated rendering

    Effects are optional. But how can you resist? :)

    Interpolation is optional. It doesn't improve rendering for all sprites, only the ones your eyes would track in a
    dynamic scene. You may have to play with some to find the right combination. But when you do it's sweet.

    The layer attribute is not used. It is provided for the convenience of external algorithms.

    The following methods and setters cause a new surface to be created: angle, scale, alpha, colorkey, flip,
    fade_effect, spin_effect, scale_effect. These images are cached in settings.image_cache. You may pre-generate the
    images and tweak the image_cache aging and memory cap behaviors to suit your performance needs.
    """
    layer = 0
    collision_type = None
    zero_transform = 0.0, 1.0, (False, False), 255, None
    srcalpha_bug_workaround = True

    def __init__(self, image, pos, angle=0.0, scale=1.0, alpha=None, colorkey=None, flip=(False, False),
                 anchor='topleft', layer=None, use_interp=True, radius=None):
        """create a SpriteFX sprite with effects capabilities

        :param image: pygame.Surface; the source image for all transforms
        :param pos: seq; (x, y) at anchor
        :param angle: float; degrees in pygame space (0 is vertical, increasing clockwise)
        :param scale: float; the zoom factor in pygame.transform.rotozoom()
        :param alpha: int; 0 to 255, alpha transparency; detects and handles SRCALPHA images
        :param colorkey: int; 0 to 255, transparent color, assumes surface alpha
        :param flip: seq; boolean arguments for pygame.transform.flip()
        :param anchor: str; the rect attribute to use when getting and setting position
        :param layer: int; user layer for sorting (not used: provided for the convenience of external algorithms)
        :param use_interp:
        """
        self.image = image
        self.rect = image.get_rect(**{anchor: pos})
        self.use_interp = use_interp
        self._source_image = image
        self._id = idgen.next()
        if self._id in id_history:
            logger.error('>>> >>> DUPLICATE ID <<< <<<')
        else:
            id_history[self._id] = 1
        self._pos = Vec2d(pos)
        self._angle = angle
        self._scale = scale
        self._alpha = alpha
        if isinstance(colorkey, str):
            colorkey = pygame.Color(colorkey)
        self._colorkey = None if colorkey is None else tuple(colorkey)
        self._anchor = anchor
        self._flip = flip
        if layer is not None:
            self.layer = layer

        self._radius = radius if radius is not None else self.rect.w // 2

        self.interpolator = interpolation.Interpolator(*self.pos)
        self.tweens = {}

        self._dirty = True
        self._dirty_pos = False

    def _get_origin(self):
        return self.rect.center
    origin = property(_get_origin)

    def _get_radius(self):
        return self._radius
    radius = property(_get_radius)

    def _get_angle(self):
        return self._angle
    def _set_angle(self, value):
        if value != self._angle:
            self._angle = value
            self._dirty = True
    angle = property(_get_angle, _set_angle, None, "rotation of surface (transform.rotozoom()) - creates a new surface")
        
    def _get_scale(self):
        return self._scale
    def _set_scale(self, value):
        if value != self._scale:
            self._scale = value
            self._dirty = True
    scale = property(_get_scale, _set_scale, None, "scale of surface (transform.rotozoom()) - creates a new surface")

    def _get_alpha(self):
        return self._alpha
    def _set_alpha(self, value):
        # if value > 255:
        #     logger.error('+++ TWEEN alpha: {}', value)
        # else:
        #     logger.debug('+++ TWEEN alpha: {}', value)
        value = max(0, value)
        value = min(255, value)
        self._alpha = value
        self.image.set_alpha(value)
        self._dirty = True
    alpha = property(_get_alpha, _set_alpha, None, "alpha of surface - creates a new surface")

    def _get_colorkey(self):
        return self._colorkey
    def _set_colorkey(self, value):
        self._colorkey = None if value is None else tuple(value)
        self._dirty = True
    colorkey = property(_get_colorkey, _set_colorkey, None, "colorkey of surface - creates a new surface")

    def _get_anchor(self):
        return self._anchor
    def _set_anchor(self, value):
        old_pos = getattr(self.rect, self._anchor)
        new_pos = getattr(self.rect, value)
        diff = Vec2d(new_pos) - old_pos
        self._anchor = value
        self._pos += diff
        self.interpolator.start += diff
        self.interpolator.end += diff
    anchor = property(_get_anchor, _set_anchor, None, "rect anchor for position - must be a valid rect attribute")

    def _get_flip(self):
        return self._flip
    def _set_flip(self, value):
        if value != self._flip:
            self._flip = value
            self._dirty = True
    flip = property(_get_flip, _set_flip, None, "flip x and y axes (transform.flip()) - creates a new surface")

    def _get_pos(self):
        return self._pos
    def _set_pos(self, val):
        self._update_pos(val)
    pos = property(_get_pos, _set_pos)

    def _get_x(self):
        return self.pos[0]
    def _set_x(self, value):
        self._update_pos((value, self._pos[1]))
    x = property(_get_x, _set_x, None, "value of x at anchor")

    def _get_y(self):
        return self.pos[1]
    def _set_y(self, value):
        self._update_pos((self._pos[0], value))
    y = property(_get_y, _set_y, None, "value of y at anchor")

    def set_zero(self, pos):
        """sets internals absolutely to pos

        The pos, x, and y attributes contain logic to help the interplator step each frame. This method circumvents the
        logic, initializing pos, x, y, interpolator.start and interpolator.end to the same value. This helps situations
        where it's desirable to move a sprite without having the visual effect of an interpolated change of position.

        :param pos: sequence; (x, y)
        :return: None
        """
        self._pos[:] = pos
        setattr(self.rect, self.anchor, pos)
        self.interpolator.set(*pos)
        self._dirty_pos = False

    def _update_pos(self, val):
        """set a new position for interpolated movement"""
        if self.use_interp:
            if not self._dirty_pos:
                self.interpolator.update(*val)
                self._dirty_pos = True
            else:
                self.interpolator.end[:] = val
        self._pos[:] = val
        setattr(self.rect, self.anchor, val)

    def hit(self, *args):
        """override"""
        pass

    def kill(self, *args):
        """override"""
        pass

    def update(self, dt):
        """override"""
        pass

    def draw(self, surface, alpha=1.0):
        """draw the sprite on surface

        :param surface: destination surface
        :param alpha: alpha for interpolated position (0.0 <= alpha <= 1.0)
        :return: None
        """
        if self._dirty:
            self._refresh_image()
        rect = self.rect
        if self.use_interp:
            rect = rect.move(*self.interpolator.raw_offset(alpha))
        surface.blit(self.image, rect)
        self._dirty_pos = False

    def _refresh_image(self):
        bucket_key = self._id  # id(self._source_image)
        transforms = abs(int(self._angle % 360)), self._scale, self._flip, self._alpha, self._colorkey
        cached_image = settings.image_cache.get(bucket_key, transforms)
        if cached_image:
            self.image = cached_image
        else:
            # transform source image
            if transforms == self.zero_transform:
                self.image = self._source_image
            else:
                self.image = self._source_image.copy()
                # BUG: smoothscale() seems to be broke in pygame 1.9.3
                # if self.angle != 0.0:
                #     # Note: pygame uses counterclockwise rotation, from SDL I guess. It hurts my brain, so I reverse it.
                #     self.image = pygame.transform.rotate(self.image, -self.angle)
                # if self.scale != 1.0:
                #     w, h = self._source_image.get_size()
                #     s = self.scale
                #     self.image = pygame.transform.smoothscale(self.image, (int(w * s), int(h * s)))
                angle = self.angle
                scale = self.scale
                if angle != 0.0 or scale != 1.0:
                    # Note: pygame uses counterclockwise rotation, from SDL I guess. It hurts my brain, so I reverse it.
                    self.image = pygame.transform.rotozoom(self.image, -angle, scale)
                if self._flip != (False, False):
                    self.image = pygame.transform.flip(self.image, *self.flip)
                # modify alpha and colorkey
                # TODO: bugs! this is a workaround for false SRCALPHA
                source_image = self._source_image
                save_alpha = source_image.get_alpha()
                # pygame surfaces are BUGGED. Grrr. Next is a kludge to handle font rendered surfaces specially.
                if isinstance(self, TextSprite):
                    # logger.error(1)
                    save_alpha = None
                if save_alpha is not None:
                    # logger.error(2)
                    source_image.set_alpha(None)
                    has_src_alpha = source_image.get_flags() & pygame.SRCALPHA
                    source_image.set_alpha(save_alpha)
                else:
                    # logger.error(3)
                    has_src_alpha = source_image.get_flags() & pygame.SRCALPHA
                if has_src_alpha:
                    # logger.error(4)
                    self.image.fill((255, 255, 255, self._alpha), None, pygame.BLEND_RGBA_MULT)
                else:
                    # logger.error(5)
                    self.image.set_alpha(self._alpha)
                    self.image.set_colorkey(self._colorkey, pygame.RLEACCEL)
            settings.image_cache.add(bucket_key, transforms, self.image)
        # TODO: the following does strange things when anchor is not 'center'
        # sync rect, pos and interpolator
        new_size = self.image.get_size()
        self_rect = self.rect
        if new_size != self_rect.size:
            center = self_rect.center
            new_rect = self.image.get_rect(center=center)
            self_anchor = self._anchor
            old_pos = getattr(self_rect, self_anchor)
            new_pos = getattr(new_rect, self_anchor)
            diff = Vec2d(new_pos) - old_pos
            # self._pos += diff
            p = self._pos
            p[0] += diff[0]
            p[1] += diff[1]
            if self.use_interp:
                # self.interpolator.start[:] += diff
                i = self.interpolator
                p = i.start
                p[0] += diff[0]
                p[1] += diff[1]
                # self.interpolator.end[:] += diff
                p = i.end
                p[0] += diff[0]
                p[1] += diff[1]
            self.rect = new_rect
        self._dirty = False

    def fade_effect(self, start_alpha=255, end_alpha=128, duration=1.0, tween_function=tweening.ease_linear, cb_end=None):
        """create a tween that updates the alpha attribute on the sprite

        This tween affects sprite.image. For images that have SRCALPHA the alpha layer is modified. Other images have
        surface alpha modified.

        :param start_alpha: int or float; starting value for the tween
        :param end_alpha: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        self.misc_effect('alpha', start_alpha, end_alpha, duration, tween_function, cb_end)
        return self

    def spin_effect(self, start_angle=0.0, end_angle=360.0, duration=1.0, tween_function=tweening.ease_linear, cb_end=None):
        """create a tween that updates the angle attribute on the sprite

        This tween affects sprite.image and sprite.rect (and sprite.pos).

        :param start_angle: int or float; starting value for the tween
        :param end_angle: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        self.misc_effect('angle', start_angle, end_angle, duration, tween_function, cb_end)
        return self

    def scale_effect(self, start_scale=1.0, end_scale=2.0, duration=1.0, tween_function=tweening.ease_linear, cb_end=None):
        """create a tween that updates the scale attribute on the sprite

        This tween affects sprite.image and sprite.rect (and sprite.pos).

        :param start_scale: int or float; starting value for the tween
        :param end_scale: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        self.misc_effect('scale', start_scale, end_scale, duration, tween_function, cb_end)
        return self

    def driftx_effect(self, start_x=0.0, end_x=0.0, duration=1.0, tween_function=tweening.ease_linear, cb_end=None):
        """create a tween that updates the x attribute on the sprite

        This tween affects sprite.x (and sprite.pos, and sprite.rect).

        :param start_x: int or float; starting value for the tween
        :param end_x: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        self.misc_effect('x', start_x, end_x, duration, tween_function, cb_end)
        return self

    def drifty_effect(self, start_y=0.0, end_y=0.0, duration=1.0, tween_function=tweening.ease_linear, cb_end=None):
        """create a tween that updates the y attribute on the sprite

        This tween affects sprite.y (and sprite.pos, and sprite.rect).

        :param start_y: int or float; starting value for the tween
        :param end_y: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        self.misc_effect('y', start_y, end_y, duration, tween_function, cb_end)
        return self

    def misc_effect(self, attr_name, start_val, end_val, duration, tween_function=tweening.ease_linear, cb_end=None):
        """create a custom tween on the sprite

        :param attr_name: str; the attribute created on this object to store the computed value
        :param start_val: int or float; starting value for the tween
        :param end_val: int or float; ending value for the tween
        :param duration: int or float; duration of the tween in seconds
        :param tween_function: see tweening module for available functions
        :param cb_end: list; callback args; see prite._cb_end() for handling the callback args
        :return: self
        """
        if cb_end is None:
            cb_end = self.tween_callback_end
        self.tweens[attr_name] = tweener.create_tween(
            self, attr_name, start_val, end_val - start_val, duration,
            tween_function, cb_end=cb_end, cb_args=(self, attr_name))
        return self

    def end_fade(self):
        """preempt and remove a tween"""
        self.end_misc('alpha')

    def end_spin(self):
        """preempt and remove a tween"""
        self.end_misc('angle')

    def end_scale(self):
        """preempt and remove a tween"""
        self.end_misc('scale')

    def end_driftx(self):
        """preempt and remove a tween"""
        self.end_misc('driftx')
        self.driftx = 0.0

    def end_drifty(self):
        """preempt and remove a tween"""
        self.end_misc('drifty')
        self.drifty = 0.0

    def end_misc(self, attr_name):
        """preempt and remove a tween"""
        if attr_name in self.tweens:
            tweener.remove_tween(self.tweens[attr_name])
            del self.tweens[attr_name]

    def tween_callback_end(self, sprite, attr_name, tween):
        """called when a tween ends, cleans up the tween
        If you supply a different callback when starting an effect, make sure to call this one or duplicate its
        function.
        
        :param sprite: sprite as returned by the tweener
        :param attr_name: attr_name  as returned by the tweener
        :param tween: tween as returned by the tweener
        :return: None
        """
        # args: see Sprite.misc_effect() and tweening.Tweener.update()
        if attr_name in self.tweens:
            del self.tweens[attr_name]


class TextSprite(SpriteFX):
    """a sprite class that provides text rendering, a number of managed tween effects and interpolated rendering 

    The layer attribute is not used by TextSprite, SpriteFX or the module. It is provided to facilitate sorting.
    """
    layer = 0

    def __init__(self, text, pos, expire=None, theme_name='floating_text', angle=0.0, scale=1.0, alpha=255,
                 colorkey=None, flip=(False, False), anchor='topleft', layer=None, use_interp=True):
        """create a text sprite
        
        expire is the duration that the text is kept alive. When sprite.alive is False, the sprite is auto-removed from
        the internal text_sprites list. The alive attribute should also be checked if the sprite is being managed
        externally. If expire is None then the text sprite is kept forever, requiring manual removal from spritefx.texts
        or spritefx.clear(do_texts=True).
        
        theme_name is the key in the settings.font_theme dict which provides the arguments for pygametext.getsurf().
        See settings.font_themes. Example theme:
            settings.font_themes = {
                'vera': {
                    'fontname': 'Vera.ttf',
                    'fontsize': 18,
                    'color': 'gold',
                    'gcolor': 'orange3',
                    'ocolor': 'black',
                    'owidth': 0.5,
                }
            }
        
        Except for text, expire, and theme_name this class accepts all the same arguments as SpriteFX. Please refer to
        SpriteFX for a further explanation of the remaining params.
        """
        self.alive = True
        self.text = text

        if layer is not None:
            self.layer = layer

        if colorkey is not None:
            if isinstance(colorkey, str):
                colorkey = pygame.Color(colorkey)
            # surf = pygametext.getsurf(text, alpha=1.0 / alpha if alpha > 0 else 0,
            #                            **settings.font_themes[theme_name])
            surf = pygametext.getsurf(text, **settings.font_themes[theme_name]).convert()
            surf.set_colorkey(colorkey, pygame.RLEACCEL)
        else:
            surf = pygametext.getsurf(text, **settings.font_themes[theme_name])
        super(TextSprite, self).__init__(surf, pos, angle, scale, alpha, colorkey, flip, anchor, layer, use_interp)

        if expire is not None:
            self.misc_effect('_expire', 1, 1, expire, cb_end=self.__expired)

        text_sprites.add(self)

    def __expired(self, *args):
        self.tween_callback_end(*args)
        self.remove()

    def remove(self):
        self.alive = False
        if self in text_sprites:
            text_sprites.remove(self)


logger.debug("imported")
