# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging
import os
import random

import pygame

from gamelib import settings
from gamelib.context_gameplay import GameplayContext
from gamelib.context_load_resources import ResourceLoaderContext, MapResourceConfig
from gamelib.gamelogic.gamelogic import GameLogic
# from gamelib.client.client import Client
# from gamelib.splash_context import SplashContext
# from gamelib.intro_context import IntroContext
# from gamelib.victory_context import VictoryContext
from gamelib.settings import cell_size
from pyknic import context
from pyknic.events import EventDispatcher
from pyknic.mathematics.geometry import SphereBinSpace2
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext
from pyknic.pyknic_pygame.resource2 import ResizeTransform
from pyknic.pyknic_pygame.sfx import MusicPlayer
from pyknic.resource2 import FakeImage, Image, DirectoryAnimation

logger = logging.getLogger(__name__)


def main():
    # put here your code
    import pyknic
    logging.getLogger().setLevel(settings.log_level)
    pyknic.logs.print_logging_hierarchy()

    random_state = random.getstate()
    logger.info(f"Set random seed state to: {random_state}")

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))
    # Dr0id is gonna hate this, I know. Doing it anyway. :)
    settings.music_player = music_player

    # todo: implement intro and such?
    # todo: initialize info
    if settings.debug_context_shim:
        # noinspection PyUnresolvedReferences,PyProtectedMember
        from gamelib._custom_context_sample import CustomContext
        space = SphereBinSpace2(cell_size)
        event_dispatcher = EventDispatcher()
        # client = Client(music_player, space, event_dispatcher)
        # mission_manager = CustomContext(client, space, event_dispatcher)
    # else:
    #     mission_manager = MissionManager()

    # splashscreen = SplashContext()
    # introscreen = IntroContext()
    # victoryscreen = VictoryContext()

    # context.push(victoryscreen)
    # context.push(mission_manager)
    # if not settings.debug_bypass_intro:
    #     context.push(introscreen)
    # if not settings.debug_bypass_splash:
    #     context.push(splashscreen)

    context.push(GameplayContext(GameLogic()))
    resizeTrans = ResizeTransform(30, 30)
    resource_def = {
        settings.resource_hero: FakeImage(25, 50, (255, 0, 0)),
        settings.resource_worm: FakeImage(50, 50, (255, 0, 255)),
        settings.resource_map1: MapResourceConfig(
            os.path.join(settings.map_dir, settings.map_file_names[settings.current_map])),
        settings.resource_cursor: Image("./data/graphics/cursor.png"),
        settings.resource_target: Image("./data/graphics/target.png"),
        settings.resource_clue_blink: Image("./data/graphics/particlePack/star_08.png"),
        # settings.resource_black_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/blacksmoke", 10, transformation=resizeTrans),
        # settings.resource_fart: DirectoryAnimation("./data/graphics/smokeparticleassets/fart", 10, transformation=resizeTrans),
        # settings.resource_white_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/WhitePuff", 10, transformation=resizeTrans),
    }
    context.push(ResourceLoaderContext(resource_def))

    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info, driver_info, wm_info):
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
