# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Settings and constants.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
from collections import namedtuple

import pygame

from gamelib.gamerender import imagecache
from gamelib.gamerender import pygametext
from pyknic.generators import GlobalIdGenerator, IdGenerator
from pyknic.pyknic_pygame.eventmapper import EventMapper, ANY_MOD

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------
#
# This file contains the distributed default settings.
#
# Don't override settings by editing this file, else they will end up in the repo.
# Instead, see the DEBUGS section at the end of this file for instructions on
# keeping your custom settings out of the repo.
#
# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------


#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0  # resulting level = master_volume * music_volume
sfx_volume = 1.0  # resulting level = master_volume * sfx_volume
mission_chatter_speed = 4.0  # seconds between emits
hero_starting_health = 100  # hero health at start of mission and healing rooms
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]
fx_enable_tactical_hud_flicker = True

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "I, Castaway"  # TODO
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------

start_info = None
air_steering_is_relative = False

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

# will be initialized by main.py
music_player = None

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 6  # default:

# Player
(
    channel_farts,
    channel_goats,
    channel_jetpack,
    channel_rumble,
    channel_5,
    channel_6,
) = range(MIXER_RESERVED_CHANNELS)

music_ended_pygame_event = pygame.USEREVENT

global_id_names = {}
GLOBAL_ID_ACTIONS = 1000
_global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids

# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------
EVT_NOISE = _global_id_generator.next("EVT_NOISE")  # (noisemaker)
EVT_KILLED = _global_id_generator.next()  # (prey)
EVT_WORM_STATE_CHANGE = _global_id_generator.next()
EVT_HERO_STATE_CHANGE = _global_id_generator.next()
EVT_GOAT_STATE_CHANGE = _global_id_generator.next()
EVT_ENTITY_REMOVED = _global_id_generator.next()
EVT_MUSIC_STARTED = _global_id_generator.next()
EVT_CLUE_FOUND = _global_id_generator.next()
EVT_RESCUE_GOAT = _global_id_generator.next()

EVT_SOUND_STARTED = _global_id_generator.next()  # sfx resource id, mode of operation
EVT_HERO_FARTED = _global_id_generator.next()
EVT_HERO_JUMPED = _global_id_generator.next()
EVT_GOAT_BAAH = _global_id_generator.next()

# actions

ACTION_TOGGLE_DEBUG_RENDER = _global_id_generator.next("ACTION_TOGGLE_DEBUG_RENDER")
ACTION_QUIT = _global_id_generator.next()

ACTION_HERO_AIM = _global_id_generator.next()

ACTION_HERO_FIRE = _global_id_generator.next()
ACTION_HERO_HOLD_FIRE = _global_id_generator.next()

ACTION_HERO_MOVE_LEFT = _global_id_generator.next()
ACTION_HERO_MOVE_RIGHT = _global_id_generator.next()
ACTION_HERO_MOVE_UP = _global_id_generator.next()
ACTION_HERO_MOVE_DOWN = _global_id_generator.next()
ACTION_HERO_STOP_LEFT = _global_id_generator.next()
ACTION_HERO_STOP_RIGHT = _global_id_generator.next()
ACTION_HERO_STOP_UP = _global_id_generator.next()
ACTION_HERO_STOP_DOWN = _global_id_generator.next()
ACTION_HERO_TRIGGER_DISCHARGE = _global_id_generator.next()
# ACTION_HERO_SWITCH = _global_id_generator.next()
# ACTION_HERO_USE = _global_id_generator.next()
# ACTION_HERO_JUMP = _global_id_generator.next()
# ACTION_HERO_FIRE_SECONDARY = _global_id_generator.next()


gameplay_event_mapper = EventMapper({
    pygame.MOUSEBUTTONDOWN: {
        1: ACTION_HERO_FIRE,  # LMB
        2: ACTION_HERO_TRIGGER_DISCHARGE,  # MMB
        3: ACTION_HERO_TRIGGER_DISCHARGE,  # RMB
        # 4: ACTION_HERO_SWITCH,  # mouse scroll up(?)
        # 5: ACTION_HERO_SWITCH,  # mouse scroll down(?)
    },
    pygame.MOUSEBUTTONUP: {
        1: ACTION_HERO_HOLD_FIRE,
        # 2: ACTION_HERO_HOLD_SECONDARY,  # MMB
        # 3: ACTION_HERO_HOLD_SECONDARY,  # RMB
    },
    pygame.MOUSEMOTION: {
        None: ACTION_HERO_AIM,
    },
    pygame.KEYDOWN: {

        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        # hero
        (pygame.K_a, ANY_MOD): ACTION_HERO_MOVE_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_HERO_MOVE_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_HERO_MOVE_UP,
        (pygame.K_s, ANY_MOD): ACTION_HERO_MOVE_DOWN,
        # (pygame.K_e, ANY_MOD): ACTION_HERO_USE,
        # (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        # (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        # (pygame.K_TAB, ANY_MOD): ACTION_HERO_SWITCH,
        # # (pygame.K_q, ANY_MOD): ACTION_HERO_SWITCH_SECONDARY,
        (pygame.K_SPACE, ANY_MOD): ACTION_HERO_TRIGGER_DISCHARGE
    },
    pygame.KEYUP: {
        # (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_a, ANY_MOD): ACTION_HERO_STOP_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_HERO_STOP_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_HERO_STOP_UP,
        (pygame.K_s, ANY_MOD): ACTION_HERO_STOP_DOWN,
        # (pygame.K_q, ANY_MOD): ACTION_HERO_HOLD_SECONDARY,
        # (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_STAND,
        # (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_STAND,
    },
    pygame.QUIT: {None: ACTION_QUIT},

})

# ------------------------------------------------------------------------------
# Kinds
# ------------------------------------------------------------------------------
KIND_HERO = _global_id_generator.next("KIND_HERO")
KIND_BACKGROUND_SPRITE = _global_id_generator.next()

KIND_ANIM_HERO_JUMP = _global_id_generator.next()
KIND_ANIM_HERO_STAND = _global_id_generator.next()
KIND_ANIM_HERO_WALK = _global_id_generator.next()
KIND_ANIM_WAVE = _global_id_generator.next()
KIND_ANIM_WAVE_SEEK = _global_id_generator.next()
KIND_ANIM_WORM_EAT = _global_id_generator.next()
KIND_ANIM_WORM_RUMBLE = _global_id_generator.next()
KIND_ANIM_GOAT = _global_id_generator.next()
KIND_ANIM_GOAT_STAND = _global_id_generator.next()
KIND_GOAT = _global_id_generator.next()
KIND_WORM = _global_id_generator.next()
KIND_SAFE_ROCK = _global_id_generator.next()
KIND_EDIBLE_ROCK = _global_id_generator.next()
KIND_CLUE = _global_id_generator.next()
KIND_START = _global_id_generator.next()
KIND_END = _global_id_generator.next()
KIND_GOAT = _global_id_generator.next()
KIND_LEVEL_BOUNDARY = _global_id_generator.next()
KIND_JETPACK = _global_id_generator.next()
KIND_HERBS = _global_id_generator.next()

# ------------------------------------------------------------------------------
# Story Milestones
# ------------------------------------------------------------------------------
INIT_MILESTONE = _global_id_generator.next('INIT_MILESTONE')
GAME_TITLE_PLAYED = _global_id_generator.next('TITLE_PLAYED')
STORY_LEVEL_1 = _global_id_generator.next('STORY_LEVEL_1')
STORY_LEVEL_2 = _global_id_generator.next('STORY_LEVEL_2')
STORY_LEVEL_3 = _global_id_generator.next('STORY_LEVEL_3')
STORY_LEVEL_4 = _global_id_generator.next('STORY_LEVEL_4')
STORY_LEVEL_5 = _global_id_generator.next('STORY_LEVEL_5')
STORY_LEVEL_6 = _global_id_generator.next('STORY_LEVEL_6')
STORY_LEVEL_7 = _global_id_generator.next('STORY_LEVEL_7')
STORY_GAME_WON = _global_id_generator.next('STORY_GAME_WON')

# Chapter 1 clues
CLUE_INTRO = _global_id_generator.next('CLUE_INTRO')
CLUE_GOAT = _global_id_generator.next('CLUE_GOAT')
CLUE_JETPACK = _global_id_generator.next('CLUE_JETPACK')
CLUE_FART_BOOST = _global_id_generator.next('CLUE_FART_BOOST')
CLUE_DIARY = _global_id_generator.next('CLUE_DIARY')
CLUE_PETROGLYPH = _global_id_generator.next('CLUE_PETROGLYPH')
CLUE_DUMMY = _global_id_generator.next('CLUE_DUMMY')  # FINAL: add new clues before this one

# when hero achieves a milestone, do: milestones.append(NEED_whatever).
# then check it like: if NEED_whatever in milestones
milestones = [INIT_MILESTONE]

# ------------------------------------------------------------------------------
# Entities
# ------------------------------------------------------------------------------

death_messages = ["Ouch!", "Try again...", "What a bite!", "Unlucky step!"]

hero_reset_interval = 3
cursor_color = (1, 85, 255)
flatulence_color = (0, 255, 0)
flatulence_critical_color = (255, 0, 0)
WormParams = namedtuple("WormParams", [
    "wander_min_interval",
    "wander_range_interval",
    "wander_direction_angle_change",
    "wander_speed",
    "seek_speed",
    "seek_dip_dist_sq",
    "dip_attack_dist_sq",
    "attack_duration",
    "kill_duration",
    "radius",
    "turn_rate",
    "give_up_min_interval",
    "give_up_max_interval",
    "max_hear_distance_sq",
    "wander_submerge_chance",
    "wander_surface_chance",
])
worm_params = WormParams(
    wander_direction_angle_change=90,
    wander_min_interval=0.5,
    wander_range_interval=4,
    wander_speed=30,
    seek_speed=100,
    seek_dip_dist_sq=50 * 50,
    dip_attack_dist_sq=40 * 40,
    attack_duration=0.5,  # rumble, last chance to get away
    kill_duration=1.8,  # hero dead if in dip_attack_dist_sq distance
    radius=25,
    turn_rate=80,  # deg per second
    give_up_min_interval=5,
    give_up_max_interval=20,
    max_hear_distance_sq=300 * 300,
    wander_surface_chance=0.001,
    wander_submerge_chance=0.001
)

HeroParams = namedtuple("HeroParams", [
    "speed",
    "jump_speed",
    "jump_height",
    "fart_angle_inaccuracy_in_deg",  # moving direction +-angle for inaccuracy
    "discharge_th_s",  # this is for the case where the jumps at the same coordinates, just how long it would be in air
    "air_steering_speed",
    'radius'
])
hero_params = HeroParams(speed=50, jump_speed=100, jump_height=2, fart_angle_inaccuracy_in_deg=45, discharge_th_s=2,
                         air_steering_speed=50, radius=20)

EnergyParams = namedtuple("EnergyParams", ["max_energy", "regeneration_rate", "min_energy_to_jump"])
energy_params = EnergyParams(max_energy=250, regeneration_rate=50, min_energy_to_jump=10)
no_energy_params = EnergyParams(max_energy=0, regeneration_rate=0, min_energy_to_jump=10)

GasOMeterParams = namedtuple("GasOMeterParams",
                             ["max_level", "critical_level", "explode_chance", "generation_rate", "range_extension"])
no_fart_params = GasOMeterParams(max_level=100, critical_level=60, explode_chance=0.01, generation_rate=0,
                              range_extension=0.5)

fart_params0 = GasOMeterParams(max_level=100, critical_level=60, explode_chance=0.003, generation_rate=2,
                              range_extension=0.5)
fart_params1 = GasOMeterParams(max_level=100, critical_level=60, explode_chance=0.0061, generation_rate=4,
                              range_extension=0.5)
fart_params2 = GasOMeterParams(max_level=100, critical_level=60, explode_chance=0.01, generation_rate=10,
                              range_extension=0.5)
flatulence = [fart_params0, fart_params1, fart_params2]


RockParams = namedtuple('RockParams', 'width height rotation')
StartParams = namedtuple('StartParams', 'x y width height')
EndParams = namedtuple('EndParams', 'x y width height')
ClueParams = namedtuple('ClueParams', 'x y width height id')

CritterParams = namedtuple("CritterParams", "turn_rate speed stand_max_duration stand_min_duration stand_turn_chance "
                                            "turn_min_duration turn_max_duration turn_stand_chance noise_interval")
critter_params = CritterParams(
    turn_rate=120,
    speed=30,
    stand_min_duration=0.5,
    stand_max_duration=3,
    stand_turn_chance=0.5,  # or else walk
    turn_min_duration=0.2,
    turn_max_duration=2,
    turn_stand_chance=0.05,  # or else walk
    noise_interval=0.1,
)

# ------------------------------------------------------------------------------
# World
# ------------------------------------------------------------------------------
cell_size = 100

# ------------------------------------------------------------------------------
# Map
# ------------------------------------------------------------------------------
map_dir = 'data/maps'
MAP_TEST_1 = _global_id_generator.next("MAP_TEST_1")
MAP_TEST_2 = _global_id_generator.next()
MAP_PLAY_1 = _global_id_generator.next()
map_file_names = {
    MAP_TEST_1: 'test_map_1.json',
    MAP_TEST_2: 'test_map_2.json',
    MAP_PLAY_1: 'test_map_3.json',
}
current_map = MAP_PLAY_1

# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 120.0  # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT  # [s]
DRAW_FPS = 60  # [fps]

# ------------------------------------------------------------------------------
# UI
# ------------------------------------------------------------------------------
_layer_gen = IdGenerator(0)
layer_background = _layer_gen.next()
layer_ground = _layer_gen.next()
layer_rocks = _layer_gen.next()
layer_worm = _layer_gen.next()
layer_above_ground = _layer_gen.next()
layer_goat = _layer_gen.next()
layer_cursor = _layer_gen.next()
layer_hero = _layer_gen.next()
layer_death_message = _layer_gen.next()
layer_clue_blink = _layer_gen.next()
layer_debug = 1000

cam_speed = 2  # hero_params.jump_speed * 0.1

# ------------------------------------------------------------------------------
# Resources
# ------------------------------------------------------------------------------
resource_hero = _global_id_generator.next("resource_hero")
resource_worm = _global_id_generator.next()
resource_map1 = _global_id_generator.next()
resource_cursor = _global_id_generator.next()
resource_target = _global_id_generator.next()
resource_clue_blink = _global_id_generator.next()
resource_black_smoke = _global_id_generator.next()
resource_fart = _global_id_generator.next()
resource_white_smoke = _global_id_generator.next()

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------
gfx_path = "data/gfx"
# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().

# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
image_cache.enable_age_cap(True)

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    # Game title
    title=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    intro1=dict(
        fontname='VeraBd.ttf',
        fontsize=35,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    intro2=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    speechbubble=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        # background=(0, 0, 0, 128)
    ),
    gamehud=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=10,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.1,
        scolor=None,
        shadow=None,
        background=(0, 0, 0, 128)
    ),
    debug=dict(
        fontname='Vera.ttf',
        fontsize=13,
        color='green4',
        # gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
)

# ------------------------------------------------------------------------------
# DEBUGS
# ------------------------------------------------------------------------------

# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
#
# Don't override debug settings in this file. Instead, do the following:
# This allows customizations for devs, and insures play-testers pulling from the
# repo will use the intended default settings.
#
# HOW TO:
# 1. Create gamelib/_custom.py.
# 2. Override your developer settings in _custom.py.
# 3. DO NOT add _custom.py to the repo. :)
#
# SAMPLE _custom.py:
# debug_quit = True
#
debug_quit = False

# Enable/disable a local developer context.
# True: invoke the shim in gamelib.main.main(); you will need to create a module
# gamelib._custom_context. See gamelib._custom_context_sample.py and gamelib.game_context.py.
#
# NOTE: >>>>> helper: cd gamelib; cp _custom_context_sample.py _custom_context.py <<<<<
# NOTE: >>>>> take care NOT to push your gamelib._custom_context.py to the repo. <<<<<
debug_context_shim = False
debug_mission_shim = False

debug_bypass_splash = False
debug_bypass_intro = False

debug_player_invincible = False

debug_skip_bg_tiles = False

debug_skip_intro = False

# noinspection PyBroadException
try:
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")
