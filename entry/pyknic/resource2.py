# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module is about resources and how they are loaded.
"""
from __future__ import print_function, division

import logging
import os

logger = logging.getLogger(__name__)
logger.debug("importing...")

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module
# TODO: use logger!


def _get_path(path):
    # todo convert assert to validation
    if '\\' in path:
        raise Exception("only forward slashes are allowed, will convert with os.path.join")  # todo use specific ex
    return os.path.join(*path.split('/'))


# requirements:
# two instances of the same transformation should be equal and produce the same 'key'
# order of transformations is relevant to be equal (in  all cases?)
class BaseTransformation(object):

    def __init__(self):
        self._transformations = []

    @property
    def key(self):
        h = hash(self)
        for t in self._transformations:
            h ^= t.key
        return h

    def transform(self, in_obj):
        for t in self._transformations:
            in_obj = t.transform(in_obj)

        return in_obj

    def then(self, other_transformation):
        assert isinstance(other_transformation, BaseTransformation), "should inherit from BaseTransformation"
        self._transformations.append(other_transformation)
        return self

    def __ne__(self, other):
        are_equal = self.__eq__(other)
        return are_equal if are_equal == NotImplemented else not are_equal

    def __eq__(self, other):
        raise NotImplementedError()

    def __hash__(self):
        raise NotImplementedError()


class AbstractResourceConfig(object):
    def __init__(self, transformation=None):  # todo AFTER_PYWEEK transformation should not be optional
        assert transformation is None or isinstance(transformation, BaseTransformation)
        self.transformation = transformation


class SpriteSheetConfig(AbstractResourceConfig):

    def __init__(self, path_to_file, tile_size, row_count, column_count, fps, frames_slice=None):
        AbstractResourceConfig.__init__(self)
        self.slice = frames_slice
        self.fps = fps
        self.path_to_file = _get_path(path_to_file)
        self.tile_size = tile_size
        self.row_count = row_count
        self.column_count = column_count


class FileList(AbstractResourceConfig):

    def __init__(self, path_to_dir, names, fps=0):
        AbstractResourceConfig.__init__(self)
        self.fps = fps
        self.path_to_dir = _get_path(path_to_dir)
        self.names = names


class Image(AbstractResourceConfig):

    def __init__(self, path_to_file):
        AbstractResourceConfig.__init__(self)
        self.path_to_file = _get_path(path_to_file)


class DirectoryAnimation(AbstractResourceConfig):

    def __init__(self, path_to_dir, fps, extension="png", transformation=None):
        AbstractResourceConfig.__init__(self, transformation)
        self.extension = extension
        self.fps = fps
        self.path_to_dir = _get_path(path_to_dir)


class FakeImage(AbstractResourceConfig):

    def __init__(self, width, height, fill_color, *fill_args):
        AbstractResourceConfig.__init__(self)
        self.width = width
        self.height = height
        self.fill_color = fill_color
        self.fill_args = fill_args


class ResourceLoaderNotFoundException(Exception):
    pass


class ResourceLoaderAlreadyExistsException(Exception):
    pass


class ResourcesStillPresentForLoaderException(Exception):
    pass


class ResourceIdAlreadyLoadedException(Exception):
    pass


class AbstractResourceLoader(object):
    # TODO: won't work! -> introduce loaded event so the resource class
    #  can register on it and know when something has been loaded
    # TODO: how to check that it is loaded only once from disk?? caching...
    def __init__(self, resource_config_type):
        self.resource_config_type = resource_config_type

    def load(self, resource_config, file_cache):  # -> Resource  # todo: here more args are needed!
        raise NotImplementedError("Abstract method!")

    def unload(self, res):  # free memory or whatever
        raise NotImplementedError("Abstract method!")


class AbstractResource(object):

    def __init__(self, resource_config):
        self.resource_config = resource_config
        self.id = "invalid"


class ResourceManager(object):  # todo is this a good name?

    def __init__(self):
        self._resource_loaders = {}  # {resource_type: loader}
        self.resources = {}  # {id: res}
        self._file_resources = {}  # {<path>: <loaded resource from disk>}  # todo use WeakValueDict?

    @property
    def resource_loaders(self):
        return self._resource_loaders.copy()

    def register_loader(self, loader, override=False):
        """
        Registers a resource loader for a specific ResourceConfig.
        :param override: if true overrides existing registrations fo that type,
                otherwise throws ResourceLoaderAlreadyExistsException
        :param loader: The loader for a ResourceConfig
        :return: None
        """
        if not isinstance(loader, AbstractResourceLoader):
            raise Exception("loader should inherit from AbstractResourceLoader")

        resource_type = loader.resource_config_type

        if resource_type in self._resource_loaders:  # todo: add a override flag?
            if override:
                logger.info("overriding loader for resource type: " + str(resource_type))  # todo unittest!
            else:
                message = "ResourceLoader already registered for type: " + str(resource_type)
                raise ResourceLoaderAlreadyExistsException(message)
        self._resource_loaders[resource_type] = loader

    def unregister_loader(self, resource_config_type):
        """
        Unregisters a loader for specific ResourceConfig type. Raises ResourcesStillPresentForLoaderException if
        there are still resources loaded through this loader in the cache (all need to be removed first).
        :param resource_config_type: The ResourceConfig type to remove.
        :return: None
        """
        assert isinstance(resource_config_type, AbstractResourceConfig) or \
               issubclass(resource_config_type, AbstractResourceConfig), \
            "resource_config_type should inherit from AbstractResourceConfig {0}".format(resource_config_type)

        if resource_config_type not in self._resource_loaders and type(
                resource_config_type) not in self._resource_loaders:
            raise ResourceLoaderNotFoundException("Could not find loader for type {0}".format(resource_config_type))

        loader = self._resource_loaders.get(resource_config_type, None)
        loader = loader if loader is not None else self._resource_loaders.get(type(resource_config_type), None)
        if loader is None:
            # todo log
            return

        # todo should those resources be unloaded automatically?
        present_resources = ["'{0}'".format(str(k)) for k, v in self.resources.items() if
                             type(v.resource_config == loader.resource_config_type)]
        if present_resources:
            ex_msg = "Unload all resources of type {0} first. Resource ids found: {1}" \
                .format(type(loader.resource_config_type), ",".join(present_resources))
            raise ResourcesStillPresentForLoaderException(ex_msg)

        self._resource_loaders.pop(resource_config_type, None)  # remove without raising an error if not present
        self._resource_loaders.pop(type(resource_config_type), None)  # remove without raising an error if not present

    def load_resource_configuration(self, resource_id, resource_config):
        """
        Load a ResourceConfiguration instance.
        :param resource_id: The resource id to store the resource in the resources cache.
        :param resource_config: The ResourceConfig instance to load.
        :return: The loaded resource (return value of the corresponding loader).
        """
        assert isinstance(resource_config,
                          AbstractResourceConfig), "resource_config should inherit from AbstractResourceConfig"
        resource_loader = self._resource_loaders.get(type(resource_config), None)  # todo: comparing with type is good??
        if resource_loader is None:
            ex_msg = "ResourceLoader for {0} not registered".format(resource_config.__class__.__name__)
            raise ResourceLoaderNotFoundException(ex_msg)
        if resource_id in self.resources:  # todo maybe just return the already loaded version?
            raise ResourceIdAlreadyLoadedException("Resource id '{0}' has already been loaded.".format(resource_id))
        resource = resource_loader.load(resource_config, self._file_resources)
        resource.id = resource_id
        assert isinstance(resource, AbstractResource), "loaded resource class should inherit from AbstractResource"
        self.resources[resource_id] = resource
        return resource

    # TODO: change to: load(self, resource_type, lazy_load, *args, **kwargs)
    # TODo: use double dispatch pattern to allow to register a member var (e.g. resources.image.load(a, b, c))

    # TODO: if lazy_load, check if the resource has been loaded, otherwise load it
    def get_resource(self, resource_id):
        """
        Return the resource stored in the resource cache under the given resource id.
        :param resource_id: The resource id to get.
        :return: The resource.
        """
        return self.resources[resource_id]

    def unload_resource(self, resource):
        """
        Remove the resource from the resource cache.
        :param resource: The resource to remove.
        :return: None
        """
        loader = self._resource_loaders.get(type(resource.resource_config), None)
        if loader is None:
            ex_msg = "Could not find resource loader '{0}' to unload the resource.".format(
                type(resource.resource_config))
            raise ResourceLoaderNotFoundException(ex_msg)  # todo maybe just log this as warning instead of an exception
        else:
            loader.unload(resource)

        self.resources.pop(resource.id, None)  # remove without exception if not present

    def unload_resource_by_id(self, resource_id):  # todo test
        the_resource = self.resources.get(resource_id, None)
        if the_resource:
            self.unload_resource(the_resource)
        else:
            pass  # todo log?

    def load_resources(self, resources_dict, progress_callback=None):
        total = len(resources_dict)
        for idx, kv in enumerate(resources_dict.items()):
            res_id, res_config = kv
            res = self.load_resource_configuration(res_id, res_config)
            if progress_callback:
                progress_callback(idx + 1, total, (idx + 1.0) / total, res_id, res)


logger.debug("imported")
